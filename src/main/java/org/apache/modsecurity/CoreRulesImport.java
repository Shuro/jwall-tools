/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.apache.modsecurity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.tools.tar.TarEntry;
import org.apache.tools.tar.TarInputStream;
import org.jwall.web.filter.ms.CoreRules;
import org.jwall.web.filter.ms.SecRule;
import org.jwall.web.filter.ms.SecRuleParser;

public class CoreRulesImport
{
    static Logger log = Logger.getLogger("CoreRulesImport");
    CoreRules rules;
    
    LinkedList<File> rfiles = new LinkedList<File>();
    
    public CoreRulesImport( URL url ) throws IOException {
        this( url.openStream() );
    }
    
    public CoreRulesImport( InputStream in ){

        try {
            rules = new CoreRules();

            TreeSet<File> files = new TreeSet<File>( new FileSorter() );
            TreeSet<File> dirs = new TreeSet<File>( new FileSorter() );
            

            File tmp = File.createTempFile("CoreRulesImport", null );
            tmp.delete();
            tmp.mkdirs();
            
            //ZipInputStream zip = new ZipInputStream( in );
            //ZipEntry entry = zip.getNextEntry();

            TarInputStream tar = new TarInputStream( new GZIPInputStream( in ) );
            TarEntry entry = tar.getNextEntry();
            while( entry != null ){

                File out = new File( tmp.getAbsolutePath() +  "/" + entry.getName() );
                if( entry.isDirectory() ){
                    out.mkdirs();
                    dirs.add( out );
                } else {
                    if( out.getName().endsWith(".conf") ){

                        if( ! out.exists() ){

                            if( ! out.getParentFile().exists() ){
                                dirs.add( out.getParentFile() );
                                out.getParentFile().mkdirs();
                            }

                            out.createNewFile();
                        }

                        FileOutputStream fos = new FileOutputStream( out );
                        tar.copyEntryContents( fos );
                        fos.flush();
                        fos.close();
                        files.add( out );
                        rfiles.add( out );
                    }
                }

                entry = tar.getNextEntry();
            }

            tar.close();
            
            SecRuleParser parser = new SecRuleParser();
            
            for( File f : files ){
                log.info("Parsing SecRules from " + f.getAbsolutePath() );
                List<SecRule> rset = parser.parse( f );
                
                rules.addAll( rset );
            }
 
            rfiles.addAll( dirs );
            rfiles.add( tmp );

        } catch (Exception e ){
            e.printStackTrace();
        }
    }
    
    
    public void cleanup(){
    	for( File f : rfiles )
    		removeFile( f );
    }
    
    public void write( OutputStream out ) throws IOException {
        CoreRules.getXStream().toXML( rules, out );
    }
    
    public String getXMLString(){
        return CoreRules.getXStream().toXML( rules );
    }


    public void removeFile( File f ){
        if( f.delete() )
            log.info("  removed " + f.getAbsolutePath() );
        else
            log.warning("  ERROR: Could not remove " + f.getAbsolutePath() );
    }
    
    class FileSorter implements Comparator<File> {

        public int compare( File f1, File f2 ){

            if( f1.getParent().equals( f2.getParent() ) ){
                int i = 1;

                while( i < f1.getName().length() && i < f2.getName().length() ){
                    if( f1.getName().substring( 0, i ) == f2.getName().substring( 0, i ) )
                        i++;
                    else
                        return f1.getName().compareTo( f2.getName() ); // f1.getName().substring(i, i).compareTo( f2.getName().substring( i, i ) );
                }
                
                return f1.getName().compareTo( f2.getName() );

            } else {
                if( f2.getAbsolutePath().startsWith( f1.getParent() ) )
                    return -1;
                else
                    return 1;
            }

        }
    }

    
    public String transform( Reader in, InputStream xslt ) throws Exception {
        TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer( new StreamSource(  xslt ) );
		
		StringWriter out = new StringWriter();
		transformer.transform( new StreamSource( in ), new StreamResult( out ) );
		out.flush();
		
		return out.toString();
    }

    
    public static void main( String args[] ){
        try {
            //URL url = new URL("http://www.modsecurity.org/download/modsecurity-core-rules_2.5-1.6.1.tar.gz");
            InputStream in = null;

            String baseDir = System.getProperty("user.home");
            
            
            in = new FileInputStream( new File( baseDir + "/modsecurity-core-rules_2.5-1.6.1.tar.gz" ) );
            
            CoreRulesImport cri = new CoreRulesImport( in );
            cri.rules.setRevision( "1.6.1" );
            
            File out = new File( baseDir + "/core-rules.xml" );
            cri.write( new FileOutputStream( out ) );

            String xml = cri.getXMLString();
            
            InputStream xslt = CoreRulesImport.class.getResourceAsStream( "/rules-html.xslt" );
            String html = cri.transform( new StringReader( xml ), xslt );
            //System.out.println( html );
            
            FileWriter w = new FileWriter( new File( baseDir + "/core-rules.html") );
            w.write( html );
            w.flush();
            w.close();
            
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
