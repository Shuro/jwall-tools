/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.solinger.sdbm;

/**
 * Java rewrite of sdbm.
 * sdbm - ndbm work-alike hashed database library
 * based on Per-Aake Larson's Dynamic Hashing algorithms. BIT 18 (1978).
 * original author: oz@nexus.yorku.ca
 * status: public domain. keep it that way.
 * @author Justin Chapweske (justin@chapweske.com)
 * @version .01 06/06/98
 * hashing routine
 */
public class Hash {

    /**
     * polynomial conversion ignoring overflows
     * [this seems to work remarkably well, in fact better
     * then the ndbm hash function. Replace at your own risk]
     * use: 65599	nice.
     *      65587   even better.
     */
    public static final int hash(String str) {
        return hash(str.getBytes());
    }

    public static final int hash(byte[] b){
        int n = 0;
        int len = b.length;

        for (int i=0;i<len;i++) {
            n = b[i] + 65599 *n;
        }
        return n;
    }
}


