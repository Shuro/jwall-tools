/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;


/**
 * <p>
 * This is a simple class for providing version information about this
 * AuditConsole.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class Version
{
    
    /**
     * Returns the version string from the AuditConsole.
     * 
     * @return
     */
    public String getVersion(){
        return Tools.VERSION;
    }
    
    
    /**
     * Returns the revision number from the AuditConsole.
     * 
     * @return
     */
    public String getRevision(){
        return extractRevision( Tools.REVISION );
    }

    
    /**
     * This method extracts the revision number from the given string. The
     * input string is expected to simply contain the subversion $Revision...$
     * format. 
     * 
     * @param revision
     * @return
     */
    protected static String extractRevision( String revision ){
        int st = revision.indexOf( " " );
        if( st < 0 )
            return "";
        int en = revision.indexOf( " ", st + 1 );
        if( en < st )
            return "";

        String rev = revision.substring( st + 1, en );
        return rev;
    }
}
