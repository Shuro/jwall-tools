/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;


/**
 * <p>
 * This class provides methods for creating a jar archive.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class JarCreator
{

    public static int BUFFER_SIZE = 10240;



    /**
     * This method recursively lists all files contained in the given directory.
     * 
     * @param dir
     * @return
     */
    public List<File> findAll( File dir ){

        List<File> files = new LinkedList<File>();

        for( File file : dir.listFiles() ){

            if( !"..".equals( file.getName() ) && !".".equals( file.getName() ) ){

                if( !file.isDirectory() )
                    files.add( file );

                if( file.isDirectory() )
                    files.addAll( findAll( file ) );

            }
        }
        return files;
    }

    /**
     * This method recursively lists all files contained in the given directory.
     * 
     * @param dir
     * @return
     */
    public List<File> findAllDirs( File dir ){

        List<File> files = new LinkedList<File>();

        if( dir.isFile() )
            return files;

        for( File file : dir.listFiles() ){

            if( !"..".equals( file.getName() ) && !".".equals( file.getName() ) ){

                if( file.isDirectory() )
                    files.add( file );

                files.addAll( findAllDirs( file ) );
            }
        }
        return files;
    }

    /**
     * This method creates a jar archive containing all files and directories
     * found within the given directory.
     * 
     * @param archiveFile
     * @param dir
     */
    public void createJarArchive( File archiveFile, File dir ){

        LinkedHashSet<File> list = new LinkedHashSet<File>( findAll( dir ) );

        File[] files = new File[ list.size() ];

        int i = 0;
        for( File file : list ){
            //System.out.println( "To be added: " + file.getAbsolutePath() );
            files[i] = file;
            i++;
        }

        createJarArchive( archiveFile, files, dir.getAbsolutePath() );
    }


    /**
     * Create a jar archive which contains the given files.
     * 
     * @param archiveFile
     * @param tobeJared
     */
    public void createJarArchive( File archiveFile, File[] tobeJared, String baseDir ){

        boolean verbose = "true".equalsIgnoreCase( System.getProperty( "verbose" ) );
        if( verbose )
            System.out.println();
        
        try {

            String base = baseDir;
            if( ! base.endsWith( File.separator ) )
                base = baseDir + File.separator;

            byte buffer[] = new byte[BUFFER_SIZE];
            // Open archive file
            FileOutputStream stream = new FileOutputStream(archiveFile);
            JarOutputStream out = new JarOutputStream(stream);


            for (int i = 0; i < tobeJared.length; i++) {
                if (tobeJared[i] == null || !tobeJared[i].exists()
                        || tobeJared[i].isDirectory())
                    continue; // Just in case...

                String fname = tobeJared[i].getAbsolutePath().replaceFirst( base, "" );
                
                if( verbose )
                    System.out.println("Adding " + fname );
                else {
                    if( i % 10 == 0 )
                        System.out.print( "." );
                }

                // Add archive entry
                JarEntry jarAdd = new JarEntry( fname );
                jarAdd.setTime(tobeJared[i].lastModified());
                out.putNextEntry(jarAdd);

                // Write file to archive
                FileInputStream in = new FileInputStream(tobeJared[i]);
                while (true) {
                    int nRead = in.read(buffer, 0, buffer.length);
                    if (nRead <= 0)
                        break;
                    out.write(buffer, 0, nRead);
                }
                in.close();
            }

            out.close();
            stream.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error: " + ex.getMessage());
        }
    }


    public void extractJarArchive( File jarFile, File dst, FilenameFilter exclude ) {
        try {

            JarFile jar = new JarFile( jarFile );


            List<JarEntry> toExtract = new LinkedList<JarEntry>(); 
            Enumeration<JarEntry> entries = jar.entries();
            while( entries.hasMoreElements() ){
                toExtract.add( entries.nextElement() );
            }

            int count = 0;
            for( JarEntry e : toExtract ){

                ZipEntry entry = jar.getEntry( e.getName() );
                File efile = new File(dst, entry.getName());

                if( entry.isDirectory() ){
                    efile.mkdirs();
                } else {

                    //System.out.println( "Extracting '" + efile.getAbsolutePath() + "'" );
                    
                    InputStream in = new BufferedInputStream(jar.getInputStream(entry));
                    OutputStream out = new BufferedOutputStream(new FileOutputStream(efile));
                    byte[] buffer = new byte[2048];

                    for (;;)  {
                        int nBytes = in.read(buffer);
                        if (nBytes <= 0) break;
                        out.write(buffer, 0, nBytes);
                    }
                    out.flush();
                    out.close();
                    in.close();
                }
                count++;
                
                if( count % 10 == 0 )
                    System.out.print( "." );
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void extractJarArchive( File jarFile, File dst ){
        this.extractJarArchive( jarFile, dst, new FilenameFilter(){
            public boolean accept( File file, String name ){
                return false;
            }
        });
    }
}
