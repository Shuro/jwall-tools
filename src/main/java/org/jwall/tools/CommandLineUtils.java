/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CommandLineUtils
{

    /**
     * Read a single number from the standard input.
     * 
     * @param msg
     * @return
     */
    public static Integer readNumber( String msg ){
        boolean read = true;

        while( read ){
            try {

                System.out.print( msg );
                BufferedReader r = new BufferedReader( new InputStreamReader( System.in ) );
                String line = r.readLine();
                Integer nr = new Integer( line );
                return nr;

            } catch ( Exception e ) {
                e.printStackTrace();
                read = true;
            }
        }
        return -1;
    }
    
    
    public static Integer cleanAndPrint( Integer clean, String msg ){
        for( int i = 0; i < clean; i++ )
            System.out.print( "\b" );
        
        System.out.print( msg );
        return msg.length();
    }
}
