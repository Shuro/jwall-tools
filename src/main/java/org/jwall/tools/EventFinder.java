package org.jwall.tools;

import java.io.File;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.filter.FilterCompiler;
import org.jwall.web.audit.filter.FilterExpression;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;

public class EventFinder implements CommandLineTool {

	FilterExpression expression;

	@Override
	public String getName() {
		return "find";
	}

	public FilterExpression compileFilter(String filter) throws Exception {
		expression = FilterCompiler.parse(filter);
		return expression;
	}

	public boolean matches(AuditEvent event) {
		return event != null && expression != null && expression.matches(event);
	}

	@Override
	public void run(String[] args) throws Exception {

		File file = new File(args[0]);

		AuditEventReader reader = new ModSecurity2AuditReader(file);

		compileFilter(args[1]);

		AuditEvent evt = reader.readNext();
		while (evt != null) {

			if (matches(evt)) {
				System.out.println(evt.get(ModSecurity.TX_ID));
			}

			evt = reader.readNext();
		}

	}
}
