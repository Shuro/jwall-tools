/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedList;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ConcurrentAuditWriter;
import org.jwall.web.audit.io.ModSecurity2AuditReader;

public class ConcurrentConverter
{

    /**
     * @param args
     */
    public static void main(String[] args)
        throws Exception
    {


        LinkedList<String> params = new LinkedList<String>();
        for( String arg : args )
            params.add( arg );

        int limit = Integer.MAX_VALUE;
        Iterator<String> it = params.iterator();
        while( it.hasNext() ){

            String p = it.next();
            if( p.startsWith( "--limit" ) ){
                it.remove();
                limit = Integer.parseInt( it.next() );
                it.remove();
            }
        }

        if( params.size() != 2 ){
            System.out.println("Usage:\n\tjava org.jwall.tools.ConcurrentConverter serial-file audit-log-directory\n" );
            return;
        }

        AuditEventReader reader = new ModSecurity2AuditReader( new File( params.removeFirst() ) );
        AuditEvent evt = reader.readNext();

        File dir = new File( params.removeFirst() );
        File index = new File( dir.getAbsolutePath() + File.separator + "index" );
        
        ConcurrentAuditWriter writer = new ConcurrentAuditWriter( dir, index );
        
        long start = System.currentTimeMillis();
        DecimalFormat fmt = new DecimalFormat( "0.00" );
        int cnt = 0;
        int skip = 0;
        
        while( evt != null && cnt < skip ){
            cnt++;
            evt = reader.readNext();
        }
        
        System.out.println( "Skipped " + cnt + " events." );
        
        while( evt != null && cnt < limit ){
            cnt++;

            writer.writeEvent( evt );
            evt = reader.readNext();

            if( cnt % 100 == 0 ){
                double time = (double) (System.currentTimeMillis() - start);
                double rate = 100.0d / (time / 1000.0d); 
                System.out.println( cnt + " events written (rate: " + fmt.format( rate ) + " evts/s  [Currently using " + memoryUsed() + " MB of memory]" );
                start = System.currentTimeMillis();
            }
        }

        reader.close();
    }
    
    public static long memoryUsed(){
        return ( Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory() ) / ( 1024 * 1024 );
    }
}
