/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Properties;

import org.jwall.Collector;
import org.jwall.log.io.SequentialFileInputStream;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.jwall.web.audit.io.ConcurrentAuditReader;
import org.jwall.web.audit.io.ConcurrentAuditWriter;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.io.ParseException;
import org.jwall.web.audit.net.AuditEventConsoleSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This class implements an <code>mlogc</code>-like tool for sending audit-log data to a remote
 * AuditConsole instance. It supports support for the original <code>mlogc</code>-configuration
 * files and additionally supports rotated index-files. 
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class Mlogc
	implements CommandLineTool
{
	/* The logger for this class */
	static Logger log = LoggerFactory.getLogger( Mlogc.class.getName() );
	
	/* The random delay */
	static Double randDelay = 0.0d;
	
	final static DecimalFormat FMT = new DecimalFormat( "0.00" );

	/* A constant for the ConsoleRoot property */
	public final static String P_CONSOLEROOT = "CONSOLEROOT";
	
	/* A constant for the ConsoleUri property */
	public final static String P_CONSOLEURI = "CONSOLEURI";
	
	/* A constant for the SensorPassword property */
	public final static String P_SENSORPASSWORD = "SENSORPASSWORD";
	
	/* A constant for the SensorUsername property */
	public final static String P_SENSORUSERNAME = "SENSORUSERNAME";


	public Mlogc(){
	}


	/**
	 * @see org.jwall.tools.CommandLineTool#getName()
	 */
	public String getName() {
		return "mlogc";
	}
	

	/**
	 * @see org.jwall.tools.CommandLineTool#run(java.lang.String[])
	 */
	public void run(String[] args) throws Exception {

		if( args.length < 1 ){
			System.err.println( "Two arguments required:   config-file  and  index-log!" );
			System.exit( -1 );
		}

		Properties pr = Mlogc.readMlogcConfig( new File( args[0] ) );
		for( Object o : pr.keySet() )
			log.debug( o + "  =  " + pr.getProperty( o.toString() ) );


		URL url = new URL( pr.getProperty( P_CONSOLEURI ) );

		String host = url.getHost();
		int port = url.getPort();

		String user = pr.getProperty( P_SENSORUSERNAME );
		String pass = pr.getProperty( P_SENSORPASSWORD );
		Collector.p.setProperty( AuditEventConsoleSender.CONSOLE_CONNECTION_SSL, url.getProtocol().equalsIgnoreCase( "https" ) + "" );
		Collector.p.setProperty( AuditEventConsoleSender.CONSOLE_CONNECTION_KEEP_ALIVE, "true" );

		log.info( "Setting CONSOLE_URI to '" + url.getPath() + "'");
		Collector.p.setProperty( AuditEventConsoleSender.CONSOLE_URI, url.getPath() );


		AuditEventConsoleSender sender = new AuditEventConsoleSender( host, port, user, pass );
		File base = new File( pr.getProperty( "COLLECTORROOT" ) );
		log.info( "Base directory is: " + base.getAbsolutePath() );

		File data = new File( pr.getProperty( "LOGSTORAGEDIR" ) );
		if( data.isAbsolute() || ! pr.getProperty( "LOGSTORAGEDIR" ).startsWith( File.separator ) )
			data = new File( base + File.separator + pr.getProperty( "LOGSTORAGEDIR" ) );
		log.info( "Using storage-directory: " + data.getAbsolutePath() );


		File file = null;
		if( args.length > 1 )
			file = new File( args[1] );
		else
			file = new File( data.getAbsolutePath() + File.separator + "index" );

		log.info( "Reading index from '" + file.getAbsolutePath() + "'" );

		if( ! file.isFile() ){
			System.err.println( "Expecting '" + file.getAbsolutePath() + "' to be an audit-event index-file!" );
			System.exit( -1 );
		}


		if( ! data.isDirectory() ){
			System.err.println( "Expecting '" + data.getAbsolutePath() + "' to be an audit-event data-directory!" );
			System.exit( -1 );
		}


		boolean sendComplete = "true".equals( System.getProperty( "send-complete" ) );
		if( sendComplete )
			log.info( "Sending complete log-file from beginning!" );

		log.info( "Reading from index '" + file + "', data-directory is '" + data + "'..." );
		AuditEventReader reader = null;

		int format = AuditFormat.guessFormat( file );
		if( format == AuditFormat.MOD_SECURITY_2_X_SERIAL_LOG ){
			log.info( "Given file is serial audit-log file, tailing and sending!" );
			reader = new ModSecurity2AuditReader( new SequentialFileInputStream( file, true ), !sendComplete);
		} else
			reader = new ConcurrentAuditReader( new SequentialFileInputStream( file, true ), data , !sendComplete );


		PrintStream errorLog = new PrintStream( new FileOutputStream( new File( data.getAbsolutePath() + File.separator + "error-queue.idx" ) ) );
		AuditEvent evt = null;
		int errors = 0;
		while( evt == null && errors < 100 ){
		    try {
		        evt = reader.readNext();
		        if( evt != null )
		            break;
		    } catch (Exception e) {
		        errors++;
		    }
		}

		long start = System.currentTimeMillis();
		int cnt = 0;

		while( evt != null ){
			cnt++;

			boolean sent = false;
			int tries = 0;
			while( ! sent && tries++ < 15){
				try {
					sender.sendAuditEvent( evt );
					if( evt.get( AuditEvent.FILE ) != null ){
						File eventFile = new File( evt.get( AuditEvent.FILE ) );
						if( !"1".equals( pr.getProperty( "KEEPENTRIES" )) ){
							log.info( "Would need to remove file: " + eventFile.getAbsolutePath() );
							eventFile.delete();
						} else {
							log.debug( "KeepEntries set to '1', not deleting event-file '{}'", eventFile.getAbsolutePath() );
						}
					}
					sent = true;
				} catch (Exception e) {
					log.error( "Failed to sent event: {}", e.getMessage() );
					try {
						log.info( "Sleeping for 10 seconds before trying to re-sent event..." );
						Thread.sleep( 10000 );
					} catch (Exception ex){
					}
				}
			}

			if( ! sent ){
				log.error( "Failed to send event (tried {} times)", tries );
				errorLog.println( ConcurrentAuditWriter.createSummary( evt ) );
			}
			
			try {
				evt = reader.readNext();
			} catch (ParseException re ){
				log.error("Failed read event, skipping this one!" );
			} catch ( Exception e) {
				log.error("Failed read event, skipping this one!" );
			}

			if( cnt % 100 == 0 ){
				double time = (double) (System.currentTimeMillis() - start);
				double rate = 100.0d / (time / 1000.0d); 
				log.info( cnt + " events send (rate: " + FMT.format( rate ) + " evts/s  [Currently using " + memoryUsed() + " MB of memory]" );
				start = System.currentTimeMillis();
			}
		}

		reader.close();
	}


	public long memoryUsed(){
		return ( Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory() ) / ( 1024 * 1024 );
	}


	public static Properties readMlogcConfig( File file ) throws Exception {
		log.debug( "Parsing mlogc config from " + file.getAbsolutePath() );
		Properties p = new Properties();

		BufferedReader r = new BufferedReader( new FileReader( file ) );
		String line = r.readLine();

		while( line != null ){
			log.debug( "line: {}", line );
			if( ! line.trim().startsWith( "#" ) && line.trim().length() > 0 ){
				String[] tok = line.trim().split( "\\s+" );
				if( tok.length == 2 ){
					String key = tok[0].trim();
					String val = tok[1].trim();
					while( val.startsWith( "\"" ) )
						val = val.substring( 1 );

					while( val.endsWith( "\"" ) )
						val = val.substring( 0, val.length() - 1 );

					p.setProperty( key.toUpperCase(), val );
				} else
					log.warn( "Weird line in mlogc-config: " + line );
			}
			line = r.readLine();
		}

		return p;
	}



	/**
	 * @param args
	 */
	public static void main(String[] args)
	throws Exception
	{
		Mlogc sender = new Mlogc();

		String[] p = new String[]{
				"/opt/modsecurity/etc/mlogc.conf",
				"/opt/modsecurity/var/audit/index"
		};

		sender.run( p );
	}
}
