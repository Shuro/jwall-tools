/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This class is simply a convenient starter for all command-line tools provided
 * by the web-audit library. New tools may be registered by adding the name and
 * the implementing class to the code.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class Tools {
	public final static String VERSION = "0.5.5";
	public final static String REVISION = "1";
	public final static String TOOL_JAR = "jwall-tools.jar";
	public static String JWALL_COMMAND = "java -jar " + TOOL_JAR;

	/** The mapping of commands to command-line tools */
	static Map<String, Class<? extends CommandLineTool>> tools = new LinkedHashMap<String, Class<? extends CommandLineTool>>();
	static Map<String, String> descriptions = new LinkedHashMap<String, String>();
	static Logger log = LoggerFactory.getLogger(Tools.class);

	static {
		tools.put("send", org.jwall.tools.EventSender.class);
		descriptions
				.put("send",
						"Allows for sending event-log-files to the AuditConsole/ModSecurity Console");

		tools.put("send-dir", org.jwall.tools.EventDirectorySender.class);
		descriptions
				.put("send-dir",
						"Sends all events found in files within a specified directory to the AuditConsole");

		tools.put("count", org.jwall.tools.EventCounter.class);
		descriptions
				.put("count",
						"Simply counting the number of events in a serial ModSecurity audit-log file");

		tools.put("stats", org.jwall.tools.AttackStatistics.class);
		descriptions
				.put("stats",
						"Count/aggregate attack statistics of a given ModSecurity audit-log file");

		tools.put("mstats", org.jwall.tools.MessageStatistics.class);
		descriptions
				.put("mstats",
						"Count/aggregate attack statistics from a series of 'Message:' lines");

		tools.put("apache2html", org.jwall.tools.ApacheConfig2Html.class);
		descriptions.put("apache2html",
				"Creates a HTML page from Apache configurations");

		tools.put("wpc", org.jwall.tools.WebPolicyCompiler.class);
		descriptions.put("wpc",
				"Compiles a web-policy definition into ModSecurity rules");

		tools.put("crs2html", org.apache.modsecurity.CoreRules2Html.class);
		descriptions.put("crs2html",
				"Create a HTML page for the core-rules set");

		tools.put("collections", org.jwall.tools.CollectionViewer.class);

		tools.put("console-update", org.jwall.tools.ConsoleUpdater.class);
		descriptions.put("console-update",
				"Allows for easily upgrading the AuditConsole");

		tools.put("console-db-check", org.jwall.tools.ConsoleDBCheck.class);
		descriptions.put("console-db-check",
				"Runs several checks for the AuditConsole database ");

		tools.put("config-tree", org.jwall.tools.ApacheConfigTree.class);
		descriptions.put("config-tree",
				"Shows the inclusion-tree of an Apache configuration");

		tools.put("config-zip", org.jwall.tools.ApacheConfigZip.class);
		descriptions.put("config-zip",
				org.jwall.tools.ApacheConfigZip.DESCRIPTION);

		tools.put("mlogc", org.jwall.tools.Mlogc.class);
		descriptions
				.put("mlogc",
						"A console-based mlogc-like sender command, running persistently");

		tools.put("convert", org.jwall.tools.AuditLogConverter.class);
		descriptions
				.put("convert",
						"A simple convertert to extract audit-log data from a audit-log file");

		tools.put("find", org.jwall.tools.EventFinder.class);
		descriptions
				.put("find",
						"Lists all event-IDs for a matching filter within an audit-log file");

		tools.put("rdns", org.jwall.tools.ReverseDNS.class);
		descriptions.put("rdns", "Create reverse lookup table from log file");

		tools.put("eval", org.jwall.tools.Evaluation.class);
		if (System.getProperty("experimental") != null) {
			descriptions.put("eval", org.jwall.tools.Evaluation.DESCRIPTION);
		}

		tools.put("pkdd2auditlog", org.jwall.tools.PkddConverter.class);
		descriptions
				.put("pkdd2auditlog",
						"Converts datasets from the PKDD2007 challenge to serial audit-log format");

		if (System.getProperty("JWALL_COMMAND") != null)
			JWALL_COMMAND = System.getProperty("JWALL_COMMAND");

		if (System.getenv("JWALL_COMMAND") != null)
			JWALL_COMMAND = System.getProperty("JWALL_COMMAND");
	}

	public static void print(URL url) {
		print(url, "");
	}

	public static void print(URL url, String prefix) {
		try {
			BufferedReader r = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String line = r.readLine();
			while (line != null) {
				System.out.print(prefix);
				System.out.println(line);
				line = r.readLine();
			}
			r.close();
		} catch (Exception e) {
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		URL logging = Tools.class.getResource("/log4j.properties");
		if (logging != null) {
			// System.err.println( "Using logging-configuration from " + logging
			// );
			PropertyConfigurator.configure(logging);
		}

		File log4j = new File("log4j.properties");
		if (log4j.isFile() && log4j.canRead()) {
			PropertyConfigurator.configure(log4j.toURI().toURL());
		}

		if (args.length == 0) {

			System.out.println();
			System.out.println();
			System.out.println("\n");
			String ver = "";
			System.out.println("  jwall-tools " + ver);
			System.out.print("  ------------");
			for (int i = 0; i < ver.length(); i++)
				System.out.print("-");
			System.out.println("\n");
			System.out
					.println("  The jwall-tools consists of an executable jar file that can be run by issuing\n");
			System.out.println("         " + JWALL_COMMAND
					+ "  COMMAND  ARGS \n");
			System.out
					.println("  where COMMAND specifies the tool you want to execute and ARGS is the list");
			System.out.println("  of parameters required for this tool.\n");
			System.out
					.println("  The following tools (commands) are available:\n");

			int max = 0;
			for (String key : tools.keySet())
				max = Math.max(max, key.length());

			max += 4;

			for (String key : tools.keySet()) {
				System.out.print("     " + key);

				int i = key.length();
				while (i++ < max)
					System.out.print(" ");

				if (descriptions.containsKey(key))
					System.out.print(descriptions.get(key));
				System.out.println();
				System.out.println();
			}

			System.out.println();
			System.out
					.println("  To see a list of options and help for the different commands, simply");
			System.out.println("  invoke the command without any parameters.");
			System.out.println();
			System.out.println();
			return;
		}

		if (args[0].startsWith("-v") || args[0].startsWith("--version")) {
			System.out.println("jwall-tools Version " + VERSION + "-"
					+ REVISION);
			System.exit(0);
		}

		for (String arg : args) {
			if (arg.startsWith("--config=")) {

				File f = new File(arg.substring("--config=".length()));
				try {
					Properties p = new Properties();
					p.load(new FileInputStream(f));
					System.getProperties().putAll(p);
				} catch (Exception e) {
					System.err.println("Failed to read settings from "
							+ f.getAbsolutePath() + ": " + e.getMessage());
					System.exit(-1);
				}
			}
		}

		List<String> params = new ArrayList<String>();
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			// System.out.println( "Processing args[" + i + "]: " + arg );
			if (arg.startsWith("-D") || arg.startsWith("--")) {
				int idx = arg.substring(2).indexOf("=");
				if (idx > 0) {
					String[] tok = arg.substring(2).split("=");
					// System.out.println( "Setting variable  " + tok[0] + " = "
					// + tok[1] );
					System.setProperty(tok[0], tok[1]);
				} else {
					System.setProperty(arg.substring(2), "true");
				}
			} else {
				params.add(arg);
			}
		}

		Class<? extends CommandLineTool> clazz = tools.get(params.remove(0));
		if (clazz != null) {

			String[] toolArgs = new String[params.size()];
			for (int i = 0; i < params.size(); i++)
				toolArgs[i] = params.get(i);

			CommandLineTool tool = clazz.newInstance();
			tool.run(toolArgs);

		} else {
			System.out.println("Unknown tool (command) with name '" + args[0]
					+ "'!");
		}
	}

	public static Properties parse(String[] params) throws Exception {
		List<String> args = new ArrayList<String>(params.length);
		for (String p : params)
			args.add(p);

		Properties p = new Properties();
		for (int i = 0; i < args.size(); i++) {
			if (args.get(i).startsWith("-") || args.get(i).startsWith("--")) {
				String name = args.get(i).replaceFirst("^--?", "");
				if (i + 1 < args.size()) {
					String val = args.get(i + 1);
					p.setProperty(name, val.trim());
				} else {
					p.setProperty(name, "true");
				}
			}
		}

		if (p.getProperty("config") != null) {
			Properties props = new Properties();
			props.load(new FileInputStream(new File(System
					.getProperty("config"))));
			for (Object k : props.keySet()) {
				p.setProperty(k.toString(), props.getProperty(k.toString()));
			}
		}
		return p;
	}

	public static List<String> handleArguments(String[] args) {
		return handleArguments(args, System.getProperties());
	}

	public static List<String> handleArguments(String[] args, Properties p) {

		for (String arg : args) {
			if (arg.equals("-v") || "--version".equals(args)) {
				System.out.println("streams, Version " + VERSION);
				return null;
			}
		}

		List<String> list = new ArrayList<String>();
		for (String arg : args) {
			if (arg.startsWith("-D") || arg.startsWith("--")) {
				int idx = arg.indexOf("=");
				String key = null;
				String value = "";
				if (idx > 2) {
					key = arg.substring(2, idx);
					value = arg.substring(idx + 1);
				} else {
					key = arg.substring(2);
				}

				log.debug("Setting property '{}' = '{}'", key, value);
				p.setProperty(key, value);
			} else {
				log.debug("Adding argument '{}'", arg);
				list.add(arg);
			}
		}

		return list;
	}
}
