/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;

import java.io.File;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import org.jwall.util.MultiSet;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventMessage;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.jwall.web.audit.io.MessageParser;

public class AttackStatistics implements CommandLineTool {

	boolean quiet = false;
	File outputFile = null;
	PrintStream out = new PrintStream( System.out );

	@Override
	public String getName() {
		return "attack-statistics";
	}

	@Override
	public void run(String[] args) throws Exception {

		if( args.length == 0 || ! (new File(args[0])).canRead() ){
			System.out.println( "You need to specify at least a file or directory!" );
			System.exit( -1 );
		}

		PrintStream out = new PrintStream( System.out );

		MultiSet<String> msgStats = new MultiSet<String>();
		MultiSet<String> ruleStats = new MultiSet<String>();
		MultiSet<String> tagStats = new MultiSet<String>();

		List<File> files = new LinkedList<File>();

		File input = new File( args[0] );
		AuditEventReader reader = null;
		if( input.isDirectory() ){
			System.out.println( "Creating list of files to read..." );
			files = EventDirectorySender.findAuditEventFiles( input, -1L );
		} else
			files.add( input );

		AuditEvent e = null;
		Date first = null;
		Date last = null;
		long start = System.currentTimeMillis();
		int count = 0;

		for( File file : files ){
			System.out.println( "Reading from file " + input );
			reader = AuditFormat.createReader( file.getAbsolutePath(), false );

			do {
				try {

					e = reader.readNext();
					if( e == null ){
						break;
					}

					if( first == null || first.after( e.getDate() ) )
						first = e.getDate();

					if( last == null || last.before( e.getDate() ) )
						last = e.getDate();

					count++;
					List<AuditEventMessage> msgs = MessageParser.parseMessages( e );
					for( AuditEventMessage msg : msgs ){
						String m = msg.getRuleMsg();
						if( m != null && ! m.trim().equals( "" ) && ! m.trim().startsWith( "Inbound Anomaly Score" ) ){
							int idx = m.indexOf( "):" );
							if( idx > 0 )
								m = m.substring( idx + 3 );

							msgStats.add( m.trim() );
						}

						if( msg.getRuleId() != null ){
							ruleStats.add( msg.getRuleId().toString() );
						}

						if( msg.getRuleTags() != null )
							for( String tag : msg.getRuleTags() )
								tagStats.add( "#" + tag );
					}

					if( count % 1000 == 0 )
						System.out.print( "." );

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} while( e != null );
			reader.close();
		}
		
		if( count == 0 ){
			System.out.println( "No events found." );
			System.exit( 0 );
		}
			

		long end = System.currentTimeMillis();
		out.println();
		SimpleDateFormat fmt = new SimpleDateFormat( "MM/dd/yyyy hh:mm" );
		out.println( count + " events processed in " + ((end-start) / 1000) + " seconds.");
		out.println( "Event date range from " + fmt.format(first) + " to " + fmt.format(last) + "." );

		TreeSet<String> rmessages = new TreeSet<String>( new CountSorter(msgStats) );
		rmessages.addAll( msgStats.getValues() );
		TreeSet<String> rules = new TreeSet<String>( new CountSorter(ruleStats) );
		rules.addAll( ruleStats.getValues() );
		TreeSet<String> tags = new TreeSet<String>( new CountSorter( tagStats ) );
		tags.addAll( tagStats.getValues() );
		out.println();
		out.println( "------------------------------------------------------" );
		out.println( "Rule Messages:" );
		print( rmessages, msgStats );

		out.println();
		out.println( "------------------------------------------------------" );
		out.println( "Rule-IDs:" );
		print( rules, ruleStats );

		out.println();
		out.println( "------------------------------------------------------" );
		out.println( "Tags:" );
		print( tags, tagStats );

		out.println( "------------------------------------------------------" );
	}


	public void print( TreeSet<String> keys, MultiSet<String> stats ){
		for( String str : keys ){
			print( stats.getCount( str ), str );
		}
	}

	public void print( Long count, String msg ){
		DecimalFormat fmt = new DecimalFormat( "0" );
		String f = fmt.format( count );
		for( int i = 0; i < 10 - f.length(); i++ ){
			print( " " );
		}
		println( f + "   " +  msg );
	}

	public void print( String str ){
		out.print( str );
	}

	public void println( String str ){
		out.println( str );
	}

	public void println(){
		out.println();
	}
	public class CountSorter implements Comparator<String> {
		MultiSet<String> cnts;
		public CountSorter( MultiSet<String> counts ){
			cnts = counts;
		}


		@Override
		public int compare(String o1, String o2) {

			if( o1 == o2 )
				return 0;

			int cmp = cnts.getCount( o1 ).compareTo( cnts.getCount( o2 ) );
			if( cmp == 0 )
				return o1.compareTo( o2 );

			return - cmp;
		}
	}
}
