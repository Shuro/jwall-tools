/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;

public class SyslogReader implements CommandLineTool, AuditEventListener {

	//Map<String,ModSecurity2AuditStream> auditStreams = new HashMap<String,ModSecurity2AuditStream>();
	Map<String,PrintStream> streams = new HashMap<String,PrintStream>();
	Pattern nameFilter = Pattern.compile( "^apache2$" ); 

	@Override
	public String getName() {
		return "syslog";
	}

	

	@Override
	public void run(String[] args) throws Exception {
		
		InputStream in = System.in;
		if( args.length > 0 && !args[0].equals( "-" ) ){
			in = new FileInputStream( new File( args[0] ) );
		}
		
		BufferedReader r = new BufferedReader( new InputStreamReader( in ) );
		String line = r.readLine();
		while( line != null ){
			System.out.println( Thread.currentThread().getName() + "  LINE: " + line );
			String[] tok = line.split( " ", 6 );
			String name = tok[4];
			String data = tok[5];

			for( int i = 0; i < tok.length; i++ ){
				//System.out.println( "tok[" + i + "] = " + tok[i] );
			}
			
			if( Pattern.matches( "^apache2:$", name ) ) {
				PrintStream out = streams.get( name );
				if( out == null ){
					
					//PipedOutputStream pos = new PipedOutputStream();
					//PipedInputStream pis = new PipedInputStream( pos );
					
					//File file = new File( "/tmp/" + name + "-audit.log" );
					//out = new PrintStream( pos ); //new FileOutputStream( file ) );
					streams.put( name, out );
					
					/*
					ModSecurity2AuditStream reader = new ModSecurity2AuditStream( pis, this, true );
					auditStreams.put( name, reader );
					Thread t = new Thread( reader );
					t.start();
					 */
				}
				out.println( data );
			}
			line = r.readLine();
		}
	}
	
	public static void main( String[] args ) throws Exception {
		new SyslogReader().run( new String[]{ "/Users/chris/syslog" } );
	}



	@Override
	public void eventArrived(AuditEvent evt) {
		if( evt != null )
			System.out.println( "Event arrived: " + evt.getEventId() );
	}



	@Override
	public void eventsArrived(Collection<AuditEvent> events) {
		for( AuditEvent e : events )
			eventArrived( e );
	}
}
