package org.jwall.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.util.MD5;

public class PkddConverter implements CommandLineTool {

	static Logger log = LoggerFactory.getLogger(PkddConverter.class);
	final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"dd/MMM/yyyy:hh:mm:ss Z");

	@Override
	public String getName() {
		return "pkdd2auditlog";
	}

	@Override
	public void run(String[] args) throws Exception {
		if (args.length != 2) {
			System.err.println("Usage:");
			System.err.println("\tjwall pkdd2auditlog  input.xml output.log");
			System.err.println();
			System.exit(-1);
		}

		File input = new File(args[0]);
		if (!input.canRead()) {
			System.err.println("Cannot open file " + input + " for reading!");
			System.exit(-1);
		}

		Integer limit = Integer.MAX_VALUE;
		try {
			limit = new Integer(System.getProperty("limit", ""
					+ Integer.MAX_VALUE));
		} catch (Exception e) {
			limit = Integer.MAX_VALUE;
		}

		InputStream in = new FileInputStream(input);
		File output = new File(args[1]);
		PrintStream p = new PrintStream(new FileOutputStream(output));
		int count = 0;
		Long t0 = System.currentTimeMillis();
		try {
			XMLInputFactory factory = XMLInputFactory.newFactory();

			XMLEventReader reader = factory.createXMLEventReader(in);
			final Map<String, String> item = new LinkedHashMap<String, String>();

			String txId = "";
			String prefix = "";
			while (reader.hasNext() && count < limit) {
				XMLEvent evt = reader.nextEvent();

				if (evt.getEventType() == XMLEvent.START_ELEMENT) {
					StartElement start = evt.asStartElement();
					final String element = start.getName().getLocalPart();

					if ("sample".equals(element)) {
						item.clear();
						txId = UUID.randomUUID().toString().toLowerCase();
						item.put("TX_ID", txId);

						@SuppressWarnings("unchecked")
						Iterator<Attribute> it = start.getAttributes();
						while (it.hasNext()) {
							Attribute a = it.next();
							item.put("@" + a.getName().getLocalPart(),
									a.getValue());
						}
					}

					if ("request".equals(element)) {
						prefix = "REQUEST";
						evt = reader.nextEvent();
						continue;
					}

					if (prefix.equals("REQUEST")) {
						if ("method".equals(element)) {
							evt = reader.nextEvent();
							String method = evt.asCharacters().getData();
							item.put(prefix + "_METHOD", method);
							continue;
						}

						if ("uri".equals(element)) {
							evt = reader.nextEvent();
							String uri = evt.asCharacters().getData();
							item.put("REQUEST_URI", uri);
							continue;
						}

						if ("protocol".equals(element)) {
							evt = reader.nextEvent();
							String proto = evt.asCharacters().getData();
							item.put("REQUEST_PROTOCOL", proto);
							continue;
						}

						if ("query".equals(element)) {
							evt = reader.nextEvent();
							String query = evt.asCharacters().getData();
							item.put("QUERY_STRING", query);
							continue;
						}
					}
					if ("headers".equals(element)) {
						evt = reader.nextEvent();
						String headers = evt.asCharacters().getData();
						item.put("REQUEST_HEADER", headers);
						continue;
					}

					if ("body".equals(element) && "REQUEST".equals(prefix)) {
						evt = reader.nextEvent();
						String body = evt.asCharacters().getData();
						item.put("REQUEST_BODY", body);
						continue;
					}
				}

				if (evt.getEventType() == XMLEvent.END_ELEMENT) {
					final String element = evt.asEndElement().getName()
							.getLocalPart();

					if ("request".equals(element)) {
						StringBuffer reqLine = new StringBuffer();
						if (item.containsKey("REQUEST_METHOD"))
							reqLine.append(item.get("REQUEST_METHOD"));

						if (item.containsKey("REQUEST_URI"))
							reqLine.append(" " + item.get("REQUEST_URI"));

						if (item.containsKey("QUERY_STRING"))
							reqLine.append("?" + item.get("QUERY_STRING"));

						if (item.containsKey("REQUEST_PROTOCOL"))
							reqLine.append(" " + item.get("REQUEST_PROTOCOL"));
						item.put("REQUEST_LINE", reqLine.toString());
						prefix = "";
						continue;
					}

					if ("sample".equals(element)) {

						String eid = MD5.md5(txId).substring(0, 8);

						p.println("--" + eid + "-A--");
						p.println("[" + dateFormat.format(new Date()) + "] "
								+ txId + " 127.0.0.1 8009 127.0.0.1 80");
						p.println("--" + eid + "-B--");
						p.println(item.get("REQUEST_LINE"));
						p.println(item.get("REQUEST_HEADER"));

						if (item.containsKey("REQUEST_BODY")) {
							p.println("--" + eid + "-C--");
							p.println(item.get("REQUEST_BODY"));
						}
						p.println("--" + eid + "-Z--\n");

						// log.debug("Writing item '{}'", txId);
						item.clear();
						count++;

						if (count % 1000 == 0) {
							log.info("{} events written.", count);
						}
					}
				}
			}

			Long t1 = System.currentTimeMillis();
			Double dur = t1.doubleValue() - t0.doubleValue();
			log.info("{} events written in {} ms.", count, (t1 - t0));
			DecimalFormat fmt = new DecimalFormat("0.000");
			log.info("{} evts/second", fmt.format(count / (dur / 1000.0)));

		} finally {
			in.close();
		}
	}

	public static void main(String[] args) {
		try {
			PkddConverter conv = new PkddConverter();
			conv.run(new String[] { "/Volumes/RamDisk/PKDD2007-Dataset.xml",
					"/Volumes/RamDisk/pkdd-audit.log" });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
