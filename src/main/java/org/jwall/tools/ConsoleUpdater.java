/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import com.thoughtworks.xstream.XStream;


/**
 * <p>
 * This is a simple update tool for upgrading the AuditConsole.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ConsoleUpdater
implements CommandLineTool
{
	/* This is the URL to check for new AuditConsole versions. */
	public static String UPDATE_URL = "http://www.jwall.org/releases?name=AuditConsole"; //"http://secure.jwall.org/AuditConsole/versions";

	public final static String VERSION = "0.4";

	public final static boolean DEBUG(){
		return "true".equals( System.getProperty( "debug" ) );
	}

	/* The usage/help text */
	public final static String USAGE = "\n\n"
		+ " Console Updater\n"
		+ " ===============\n\n"
		+ " This tool is intended  to ease the update  of your AuditConsole.  Use of\n"
		+ " the ConsoleUpdater is currently experimental.\n\n"
		+ " To update your existing AuditConsole simply call this tool with the full\n"
		+ " path to your AuditConsole install directory, e.g.\n"
		+ "\n"
		+ "     java -jar ConsoleUpdater.jar /opt/AuditConsole \n"
		+ "\n"
		+ " The updater will then determine your current version of the AuditConsole\n"
		+ " and check for an updated version. Before upgrading your Console, it will\n"
		+ " ask for confirmation and backup the old version.\n"
		+ "\n"
		+ " IMPORTANT: Use of this  updater is  experimental and does currently only\n"
		+ "            work for updating the standalone version of the AuditConsole.\n"
		+ "\n";



	/**
	 * This method checks whether the denoted file is a directory containing the console application,
	 * i.e. it contains a <code>WEB-INF</code> subdirectory with a <code>AuditConsole.properties</code>
	 * file.
	 * 
	 * TODO: We should check for <code>WEB-INF/lib/AuditConsole*.jar</code> instead of 
	 *       <code>AuditConsole.properties</code> to catch unconfigured AuditConsoles as well.
	 * 
	 * @param file
	 * @return
	 */
	public static boolean isConsoleAppDir( File file ){

		if( DEBUG() )
			System.out.println( "Checking for  isConsoleAppDir( '" + file.getAbsolutePath() + "' )" );

		if( ! file.isDirectory() )
			return false;

		for( File f : file.listFiles() ){

			if( f.isDirectory() ){

				File lib = new File( f.getAbsolutePath() + File.separator + "lib" );
				if( lib != null && lib.isDirectory() ){
					for( File jar : lib.listFiles() ){
						if( DEBUG() )
							System.out.println( "Checking for file " + jar.getAbsolutePath() + " => name =" + jar.getName() );
						if( jar.getName().matches( "AuditConsole-(.*).jar" ) || jar.getName().startsWith( "console-api" ) )
							return true;
					}
				}
			}
		}

		//System.err.println( "Not a console-app directory: '" + file.getAbsolutePath() + "'" );
		return false;
	}



	/**
	 * Extracts the version from the given directory. If the directory does not contain
	 * the console-application, this method will return <code>null</code>.
	 * 
	 * @param file
	 * @return
	 */
	public static String determineVersion( File file ){

		if( !isConsoleAppDir( file ) ){

			return null;
		}


		try {

			File manifest = new File( file.getAbsolutePath() + File.separator + "META-INF" + File.separator + "MANIFEST.MF" );
			if( !manifest.canRead() ){
				System.err.println( "[Error]   Failed to read the manifest file!" );
				return null;
			}

			Map<String,String> map = new LinkedHashMap<String,String>();

			BufferedReader r = new BufferedReader( new FileReader( manifest ) );
			String line = r.readLine();
			while( line != null ){
				if( line.indexOf( ":" ) >= 0 ){
					String[] tok = line.split( ":", 2 );
					map.put( tok[0].trim(), tok[1].trim() );
					if( DEBUG() )
						System.out.println( "  " + tok[0] + " = " + tok[1] );
				}
				line = r.readLine();
			}

			r.close();

			return map.get( "Product-Version" );

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public List<File> findConsoleDirectories( File file ){

		List<File> files = new LinkedList<File>();

		if( file.isDirectory() ){

			for( File f : file.listFiles() ){

				if( f.isDirectory() && isConsoleAppDir( f ) ){
					files.add( f );
				} else {
					if( f.isDirectory() && ! f.getName().equalsIgnoreCase( "var" ) )
						files.addAll( findConsoleDirectories( f ) );
				}
			}
		}

		return files;
	}

	public File download( Release r ) throws Exception {

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, new TrustManager[]{ new ZeroTrustManager() }, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
		}

		URL url = new URL( r.updateUrl );

		File tempFile = File.createTempFile( "AuditConsole-"+r.getRevision(), ".war" );
		tempFile.deleteOnExit();

		if( DEBUG() )
			System.out.println( "[Ok]     Downloading to " + tempFile.getAbsolutePath() );

		System.out.print( "[Ok]     Downloading release " + r.getVersion() + "r" + r.getRevision() );
		if( DEBUG() )
			System.out.print( " from " + r.updateUrl );
		System.out.print( "   " );
		FileOutputStream writer = new FileOutputStream( tempFile );
		InputStream in = url.openStream();
		byte[] buf = new byte[8192];

		int clean = 0;
		int total = 0;
		int read = 0;
		do {

			read = in.read( buf );
			total += read;
			if( read > 0 ){
				//System.out.println( "read " + read + " bytes..." );
				writer.write( buf, 0, read );
			}

			if( total % (1024) == 0 )
				clean = CommandLineUtils.cleanAndPrint( clean,  (total / 1024 ) + "K");

		} while( read > 0 );

		writer.flush();
		writer.close();
		in.close();

		System.out.println( "]");
		return tempFile;
	}



	public Map<String,String> readManifest( File file ){
		try {

			File manifest = new File( file.getAbsolutePath() + File.separator + "META-INF" + File.separator + "MANIFEST.MF" );
			if( !manifest.canRead() ){
				System.err.println( "[Error]  Failed to read the manifest file '" + manifest.getAbsolutePath() + "'" );
				return null;
			}

			Map<String,String> map = new LinkedHashMap<String,String>();

			BufferedReader r = new BufferedReader( new FileReader( manifest ) );
			String line = r.readLine();
			while( line != null ){
				if( line.indexOf( ":" ) >= 0 ){
					String[] tok = line.split( ":", 2 );
					map.put( tok[0].trim(), tok[1].trim() );
					//System.out.println( tok[0] + " = " + tok[1] );
				}
				line = r.readLine();
			}

			r.close();

			return map;

		} catch (Exception e) {
			e.printStackTrace();
			return new HashMap<String,String>();
		}
	}


	/**
	 * 
	 * 
	 * @param file
	 * @return
	 */
	public File backupAppDir( File file ){

		if( !isConsoleAppDir( file ) ) {

		}

		File backup = new File( "/tmp/console-backup.war" );
		JarCreator jarCreator = new JarCreator();
		jarCreator.createJarArchive( backup, file );
		return backup;
	}


	public void cleanAppDir( File file ){

		JarCreator c = new JarCreator();
		List<File> files = c.findAll( file );

		List<File> dirs = new LinkedList<File>();

		for( File f : files ){

			if( ! f.getName().equals( "AuditConsole.properties" ) ){
				//System.out.println( "Deleting file " + f.getAbsolutePath() );
				f.delete();
			} else
				System.out.print( "preserving '" + f.getName() + "'" );

			if( f.isDirectory() )
				dirs.add( f );
		}

		dirs = c.findAllDirs( file );

		for( File dir : dirs ){
			//System.out.println( "Need to delete directory: " + dir.getAbsolutePath() );
			dir.delete();
		}
	}



	public void run(String[] args) throws Exception
	{
		if( DEBUG() ){
			System.out.print( "args:" );
			for( String arg : args ){
				System.out.print( " " + arg );
			}
			System.out.println();
		}

		if( args.length < 1 ){

			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println( "  ConsoleUpdater v" + VERSION );
			System.out.println( "  -------------------\n" );
			System.out.println( "  This tool is intended  to ease the update  of your AuditConsole.  Use of" );
			System.out.println( "  the ConsoleUpdater is currently experimental.\n" );
			System.out.println( "  To update your existing AuditConsole simply call this tool with the full" );
			System.out.println( "  path to your AuditConsole install directory, e.g." );
			System.out.println();
			System.out.println( "  \tjava -jar " + Tools.TOOL_JAR + "  console-update  /opt/AuditConsole" );
			System.out.println();
			System.out.println( "  The updater will then determine your current version of the AuditConsole" );
			System.out.println( "  and check for an updated version. Before upgrading your Console, it will" );
			System.out.println( "  ask for confirmation and backup the old version." );
			System.out.println();
			System.out.println( "  IMPORTANT: Use of this  updater is  experimental and does currently only" );
			System.out.println( "             work for updating the standalone version of the AuditConsole." );
			System.out.println();
			System.out.println();
			return;
		}

		System.out.println( "\n\n" );

		try {
			if( System.getProperty( "update.url" ) != null ){
				URL url = new URL( System.getProperty( "update.url" ) );
				UPDATE_URL = url.toString();
			}
		} catch (Exception e) {
			System.err.println( "[Error]  Invalid update-url: " + System.getProperty( "update.url" ) + "!" );
			System.exit( -1 );
		}


		System.out.println( "\n\n" );
		System.out.println( "  ConsoleUpdater v" + VERSION );
		System.out.println( "  ===================\n" );

		if( DEBUG() )
			System.out.println( "Using UPDATE_URL = " + UPDATE_URL );

		File appDir = new File( args[0] );
		if( ! appDir.canRead() ){
			System.out.println( "[Error]  Cannot open configuration file '" + appDir.getAbsolutePath() + "' for reading!" );
			System.exit( -1 );
		}

		File lock = new File( appDir.getAbsolutePath() + File.separator + "var" + File.separator + "data" + File.separator + ".lock" );
		if( lock.exists() ){
			System.out.println();
			System.out.println( "  The directory " + appDir.getAbsolutePath() + " is locked by an AuditConsole instance!" );
			System.out.println();
			System.out.println( "     Lock-file: '" + lock.getAbsolutePath() + "'" );
			System.out.println();
			System.out.println( "  You need to shut down the AuditConsole before running the console-updater.  If the" );
			System.out.println( "  AuditConsole is not running or has been shut down uncleanly, then delete the lock-" );
			System.out.println( "  file and start the console-update again." );
			System.out.println();
			System.exit( -1 );
		}


		File consoleAppDir = new File( appDir.getAbsolutePath() + File.separator + "lib" + File.separator + "console" );


		ConsoleUpdater updater = new ConsoleUpdater();
		List<File> appDirs = new LinkedList<File>();
		if( appDir.isDirectory() )
			appDirs = updater.findConsoleDirectories( appDir );

		if( appDirs.size() > 1 ){

			System.out.println( "Found the following console-app directories:\n" );

			for( int i = 1; i - 1 < appDirs.size(); i++ )
				System.out.println( "  " + i + ")  " + appDirs.get( i - 1 ) );

			System.out.println();

			Integer id = Integer.MAX_VALUE;

			while( id - 1 > appDirs.size() ){
				id = CommandLineUtils.readNumber( "Please choose the directory you want to update (0 for cancel): " );
				if( id < 1 ){
					System.out.println( "User cancelled. Aborting update...");
					System.out.println();
					System.exit( -1 );
				}
			}
			System.out.println( "\n\n" );
			consoleAppDir = appDirs.get( id - 1 );
		} else {

			if( appDirs.size() == 1 ){
				consoleAppDir = appDirs.get( 0 );
			} else {
				System.err.println( "[Error] No console-app directory found!" );
				System.exit( -1 );
			}

		}

		if( !isConsoleAppDir( consoleAppDir ) ){
			System.err.println( "[Error]  Not a console-app directory: '" + consoleAppDir.getAbsolutePath() + "'" );
			System.exit( -1 );
		}

		File downloaded = null;

		if( args.length > 1 ){

			downloaded = new File( args[1] );
			if( ! downloaded.isFile() ){
				System.out.println( "[Error]  The specified update file does not exist!" );
				System.exit( -1 );
			} else {
				System.out.println( "[Ok]   Using file '" + downloaded.getAbsolutePath() + "' for update." );
			}

		} else {


			String version = determineVersion( consoleAppDir );
			Integer revision = 481;

			Map<String,String> mf = updater.readManifest( consoleAppDir ); 

			System.out.println( "[Ok]    Your AuditConsole version is " + version );
			System.out.println( "[Ok]    Checking for updates..." );


			Date released = null;
			SimpleDateFormat fmt = new SimpleDateFormat( "MMMM dd yyyy", Locale.ENGLISH );

			try {
				String dateString = mf.get( "Created-At" );
				if( dateString != null ){
					released = fmt.parse( dateString );
					System.out.println( "date-string is " + dateString + " ~> " + released );
				} else {

					if( mf.get( "Product-Build" ) != null && mf.get( "Product-Build" ).length() > 7 ){

						SimpleDateFormat df = new SimpleDateFormat( "yyyyMMdd" );
						released = df.parse( mf.get( "Product-Build" ).substring( 0, 8 ) );

					}

				}
			} catch (Exception pe ){
			}

			if( released == null ){
				System.out.println( "Failed to determine the release-date of your current AuditConsole!" );
				released = new Date( 0L );
			}

			URL url = new URL( UPDATE_URL );


			XStream xs = new XStream();
			xs.processAnnotations( Release.class );
			xs.processAnnotations( Releases.class );

			Releases rs = (Releases) xs.fromXML( url.openStream() );

			Map<Integer,Release> rel = new LinkedHashMap<Integer,Release>();
			int i = 1;

			System.out.println();
			System.out.println( "-------------------------------------------" );
			System.out.println( "  Found the following releases to upgrade to:" );
			System.out.println();

			for( Release r : rs.releases ){

				if( r.getReleased().after( released ) || r.getRevision().compareTo( revision.toString() ) > 0 ){
					System.out.print( " " + i + ")  Version " + r.getVersion() + ", rev " + r.getRevision() );

					if( "true".equals( System.getProperty( "debug" ) ) )
						System.out.print( ", update-url is " + r.updateUrl );

					System.out.println();
					rel.put( new Integer( i ), r );
					i++;
				}
			}

			System.out.println();

			BufferedReader r = new BufferedReader( new InputStreamReader( System.in ) );

			String upgrade = "";
			Integer sel = -1;

			while( sel != 0 || sel < 0 || ! rel.containsKey( sel ) ) {
				System.out.print( "  Please choose the version you'd like to upgrade to (enter '0' to cancel): " );
				upgrade = r.readLine();
				try {
					sel = new Integer( upgrade );
				} catch (Exception e ) {
					e.printStackTrace();
					sel = -1;
				}

				if( sel == 0 ){
					System.out.println( "\n[Ok]     Aborting console updater...\n" );
					System.exit( -1 );
				}

				if( rel.containsKey( sel ) )
					break;
			}

			System.out.println();
			System.out.println();

			Release upgradeRelease = rel.get( sel );
			downloaded = updater.download( upgradeRelease );
			if( ! "true".equals( System.getProperty( "checksum.skip" ) )  ){

				String md5 = FileCheck.md5( downloaded );
				if( DEBUG() )
					System.out.println( "[Ok]     MD5 sum of downloaded file is " + md5 );

				if( !md5.equalsIgnoreCase( upgradeRelease.getChecksum() ) ){

					System.out.println();
					System.out.println( "[Error]  MD5 Checksum does not match the release information: release-checksum is " + upgradeRelease.getChecksum() );
					System.out.println();
					System.out.println( "[Error]  Upgrade will be aborted. If you still want to upgrade to that release, try using the option  '-Dchecksum.skip=true' ");
					System.out.println();
					System.exit( -1 );
				} else {
					System.out.println( "[Ok]     Checksum matches downloaded file." );
				}
			} else
				System.out.println( "File checksum skipped." );
		}


		System.out.print( "[Ok]     Creating backup:                 [" );
		File backup = updater.backupAppDir( consoleAppDir );
		System.out.println( "]" );
		System.out.println( "[Ok]     Wrote backup of console-app to '" + backup.getAbsolutePath() + "'" );


		//
		//
		System.out.println();
		System.out.print( "[Ok]     Cleaning current console-app directory... " );
		updater.cleanAppDir( consoleAppDir );
		System.out.println();


		System.out.print( "[Ok]     Extracting new version   [" );
		JarCreator c = new JarCreator();
		c.extractJarArchive( downloaded, consoleAppDir );
		System.out.println( "]" );
		System.out.println( "[Ok]     Upgrade finished.\n");
		System.out.println( "[Ok]     Please start your AuditConsole and check for the new version!" );
		System.out.println();
	}



	public String getName()
	{
		return "update";
	}
}
