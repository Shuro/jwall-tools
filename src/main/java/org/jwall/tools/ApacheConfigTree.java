/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;

import java.io.File;

import org.jwall.apache.httpd.config.ApacheConfig;
import org.jwall.apache.httpd.config.ConfigParser;
import org.jwall.apache.httpd.config.Directive;

public class ApacheConfigTree implements CommandLineTool {
	boolean verbose = false;

	public ApacheConfigTree() {

		verbose = "true".equalsIgnoreCase(System.getProperty("verbose"));

	}

	public String getName() {
		return "config-tree";
	}

	public void run(String[] args) throws Exception {

		File file = new File(args[0]);

		ConfigParser parser = new ConfigParser(file);
		ApacheConfig config = parser.parseConfig();

		walk(config, 0, null);

	}

	public void walk(ApacheConfig cfg, int depth, ApacheConfig parent) {

		StringBuffer prefix = new StringBuffer();

		for (int i = 0; i < depth; i++)
			prefix.append("   ");

		/*
		 * if( parent != null && cfg.getIncludedBy() != null ){
		 * //System.out.println( prefix.toString() + "       parent=" + parent +
		 * ", included by=" + cfg.getIncludedBy() ); String incAt =
		 * cfg.getIncludedBy(); int idx = incAt.lastIndexOf( ":" ); Directive
		 * inclusion = getDirectiveAtLine( parent, new Integer(
		 * cfg.getIncludedBy().substring( idx + 1 ) ) ); //if( inclusion != null
		 * ) // System.out.println( "    inclusion is '" +
		 * inclusion.toPlainTxt() + "', location: " + inclusion.getLocation() );
		 * } //else //System.out.println( prefix.toString() + "       parent=" +
		 * parent );
		 */

		System.out.print("I" + prefix.toString() + cfg.getFile()); // +
																	// "  (Included at '"
																	// +
																	// cfg.getIncludedBy()
																	// ); // +
																	// "',  last modified at '"
																	// + new
																	// Date(cfg.lastModified())
																	// + "'), "
																	// );

		if (verbose && cfg.getIncludedBy() != null)
			System.out.print("   (Included at '" + cfg.getIncludedBy() + "')");
		System.out.println();

		for (File file : cfg.getReferencedFiles()) {
			System.out
					.println("R" + prefix.toString() + file.getAbsolutePath());
		}

		for (ApacheConfig child : cfg.getSubTrees()) {
			walk(child, depth + 1, cfg);
		}
	}

	public Directive getDirectiveAtLine(ApacheConfig config, Integer line) {
		for (Directive d : config.getChildren()) {
			if (d.getLocation().getLine().equals(line))
				return d;
		}

		return null;
	}

	public static void main(String[] args) throws Exception {

		String[] params = new String[] { "/www/apache2/conf/httpd.conf" };

		if (params.length != 1) {
			System.out.println();
			System.out
					.println("You need to specify the top-level Apache configuration file!");
			System.out.println();

			System.out.println("Usage:\n\tjava -jar " + Tools.TOOL_JAR
					+ " config-tree /path/to/httpd.conf");
			System.out.println();

			System.exit(-1);
		}

		System.out.println();
		ApacheConfigTree act = new ApacheConfigTree();
		act.run(params);
		System.out.println();
	}
}
