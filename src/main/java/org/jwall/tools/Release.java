/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;


import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;


/**
 * <p>
 * This is a simple class for providing version information about this
 * AuditConsole.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias("Release")
public class Release
    implements Comparable<Release>
{
    @XStreamAsAttribute
    Integer id;

    @XStreamAlias("ProductName")
    String productName;
    
    @XStreamAlias("ProductVersion")
    String version;
    
    @XStreamAlias("ProductRevision")
    String revision;
    
    @XStreamAlias("ProductUrl")
    String productUrl;
    
    @XStreamAlias("ReleaseDate")
    Date released;
    
    @XStreamAlias("UpdateUrl")
    String updateUrl;
    
    @XStreamAlias("Checksum")
    String checksum;
    
    
    public Release( String name, String ver, String rev, Date releaseDate ){
        this.productName = name;
        this.version = ver;
        this.revision = rev;
        this.released = releaseDate;
    }


    public Release( String name, String ver, String rev, Date releaseDate, String url ){
        this( name, ver, rev, releaseDate );
        this.productUrl = url;
    }
    
    
    
    /**
     * @return the version
     */
    public String getVersion()
    {
        return version;
    }


    /**
     * @param version the version to set
     */
    public void setVersion(String version)
    {
        this.version = version;
    }


    /**
     * @return the revision
     */
    public String getRevision()
    {
        return revision;
    }


    /**
     * @param revision the revision to set
     */
    public void setRevision(String revision)
    {
        this.revision = revision;
    }


    /**
     * @return the released
     */
    public Date getReleased()
    {
        return released;
    }


    /**
     * @param released the released to set
     */
    public void setReleased(Date released)
    {
        this.released = released;
    }
    

    /**
     * @return the checksum
     */
    public String getChecksum()
    {
        return checksum;
    }


    /**
     * @param checksum the checksum to set
     */
    public void setChecksum(String checksum)
    {
        this.checksum = checksum;
    }


    public int compareTo( Release other ){
        return this.released.compareTo( other.released );
    }
    
    
    public static void main( String[] args )
        throws Exception
    {
        
        SimpleDateFormat fmt = new SimpleDateFormat( "dd.MM.yyyy" );
        List<Release> versions = new LinkedList<Release>();
        
        versions.add( new Release( "AuditConsole", "0.3", "", fmt.parse( "08.11.2009" ), "http://www.jwall.org/AuditConsole" ) );
        versions.add( new Release( "AuditConsole", "0.3b", "", fmt.parse( "16.11.2009" ), "http://www.jwall.org/AuditConsole" ) );;
        versions.add( new Release( "AuditConsole", "0.3.1", "r482", fmt.parse( "15.12.2009" ), "http://www.jwall.org/AuditConsole" ) );;
        //versions.add( new Release( "AuditConsole", "0.3.2", "", fmt.parse( "16.11.2009" ), "http://www.jwall.org/AuditConsole" ) );;
        
        XStream xs = new XStream();
        xs.processAnnotations( Release.class );
     
        FileOutputStream out = new FileOutputStream( new File("/Users/chris/versions.xml" ) );
        for( Release v : versions ){
            xs.toXML( v, out );
            out.write( '\n' );
        }
        
        out.flush();
        out.close();
    }
}
