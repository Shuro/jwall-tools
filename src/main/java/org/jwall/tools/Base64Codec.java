package org.jwall.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Base64Codec
    implements CommandLineTool
{

    @Override
    public String getName()
    {
        return "b64codec";
    }

    @Override
    public void run(String[] args) throws Exception
    {
        if( args.length <= 0){
            System.err.println( "Missing argument! You need to specify a file to decode!" );
        }
        
        File file = new File( args[0] );
        BufferedReader r = new BufferedReader( new FileReader( file ) );
        StringBuffer buf = new StringBuffer();
        String line = r.readLine();
        while( line != null ){
            buf.append( line );
            buf.append( "\n" );
            line = r.readLine();
        }
        r.close();
        org.jwall.web.audit.util.Base64Codec b64 = new org.jwall.web.audit.util.Base64Codec();
        byte[] decoded = b64.decode( buf.toString().getBytes() );
        System.out.println( new String( decoded ) );
    }
}
