/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;

import org.jwall.apache.httpd.config.ApacheConfig;
import org.jwall.apache.httpd.config.ConfigParser;
import org.jwall.apache.httpd.config.Directive;

public class ApacheConfigZip implements CommandLineTool {
	boolean verbose = false;

	public final static String DESCRIPTION = "This command allows for storing all files referenced by the httpd.conf in a ZIP archive";

	public static int BUFFER_SIZE = 10240;

	public ApacheConfigZip() {

		verbose = "true".equalsIgnoreCase(System.getProperty("verbose"));

	}

	public String getName() {
		return "config-zip";
	}

	public void run(String[] args) throws Exception {
		if (args.length != 2) {
			System.out.println();
			System.out
					.println("You need to specify the top-level Apache configuration file and the name of the archive where you want to store the configuration!");
			System.out.println();

			System.out.println("Usage:\n\tjava -jar " + Tools.TOOL_JAR
					+ " config-zip /path/to/httpd.conf  DESTINATION.zip");
			System.out.println();

			System.exit(-1);
		}

		File file = new File(args[0]);
		File archive = new File(args[1]);
		if (!args[1].toLowerCase().endsWith(".zip"))
			archive = new File(args[1] + ".zip");

		ConfigParser parser = new ConfigParser(file);
		ApacheConfig config = parser.parseConfig();

		Set<File> files = new LinkedHashSet<File>();
		files.add(file);
		files.addAll(walk(config, 0, null));

		File[] toStore = new File[files.size()];
		int i = 0;
		for (File f : files) {
			toStore[i++] = f;
		}

		System.out.println("Creating zip archive:");
		createJarArchive(archive, toStore);
		System.out.println("Stored " + files.size() + " files in "
				+ file.getAbsolutePath() + ".");
	}

	public Set<File> walk(ApacheConfig cfg, int depth, ApacheConfig parent) {

		LinkedHashSet<File> files = new LinkedHashSet<File>();
		files.add(cfg.getFile());

		for (ApacheConfig child : cfg.getSubTrees())
			files.addAll(walk(child, depth + 1, cfg));

		return files;
	}

	public Directive getDirectiveAtLine(ApacheConfig config, Integer line) {
		for (Directive d : config.getChildren()) {
			if (d.getLocation().getLine().equals(line))
				return d;
		}

		return null;
	}

	/**
	 * Create a jar archive which contains the given files.
	 * 
	 * @param archiveFile
	 * @param tobeJared
	 */
	public void createJarArchive(File archiveFile, File[] tobeJared) {

		try {

			byte buffer[] = new byte[BUFFER_SIZE];
			// Open archive file
			FileOutputStream stream = new FileOutputStream(archiveFile);
			JarOutputStream out = new JarOutputStream(stream);

			for (int i = 0; i < tobeJared.length; i++) {
				if (tobeJared[i] == null || !tobeJared[i].exists()
						|| tobeJared[i].isDirectory())
					continue; // Just in case...

				String fname = tobeJared[i].getAbsolutePath();

				System.out.println("  Adding " + fname + " ... ("
						+ ((int) (tobeJared[i].length() / 1024)) + "kB)");

				// Add archive entry
				JarEntry jarAdd = new JarEntry(fname);
				jarAdd.setTime(tobeJared[i].lastModified());
				out.putNextEntry(jarAdd);

				// Write file to archive
				FileInputStream in = new FileInputStream(tobeJared[i]);
				while (true) {
					int nRead = in.read(buffer, 0, buffer.length);
					if (nRead <= 0)
						break;
					out.write(buffer, 0, nRead);
				}
				in.close();
			}

			out.close();
			stream.close();

		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("Error: " + ex.getMessage());
		}
	}

	public static void main(String[] args) throws Exception {

		String[] params = new String[] { "/www/apache2/conf/httpd.conf",
				"/tmp/apache-config.zip" };

		ApacheConfigZip act = new ApacheConfigZip();
		act.run(params);
		System.out.println();
	}
}
