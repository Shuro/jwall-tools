package org.jwall.tools;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.jwall.util.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * This class implements a simple loop which opens a URL, reads the page and goes to
 * sleep for a specified amount of time. The is will restart.
 * </p>
 * <p>
 * The purpose of this class is to simulate a slowly requesting client to test rules
 * for limitting request per IP address.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class PeriodicFetcher
	extends Thread
{
	static Logger log = LoggerFactory.getLogger( PeriodicFetcher.class );
	boolean running = false;
	List<Client> clients;
	DateFormat fmt = new SimpleDateFormat( "dd.MM.yyyy HH:mm:ss" );
	
	public PeriodicFetcher( Collection<Client> clients ){
		this.clients = new ArrayList<Client>( clients );
	}
	
	
	
	
	/**
	 * @see java.lang.Thread#run()
	 */
	public void run(){
	
		running = true;
		
		List<Thread> threads = new ArrayList<Thread>();
		for( Client c : clients ){
			Thread t = new Thread( c );
			threads.add( t );
		}

		log.info( "Starting client-threads ({} threads)", threads.size() );
		for( Thread t : threads )
			t.start();
		
		Long time = System.currentTimeMillis();
		Long offset = time % 1000;
		log.info( "Waiting for {} ms", offset );
		try {
			Thread.sleep( offset );
		} catch (Exception e){}
		
		
		while( running ){
			Long now = System.currentTimeMillis();
			//log.info( "Fetching statistics for {}", now );
			String date = fmt.format( new Date( now ) );
			
			List<Map<String,Double>> statistics = new ArrayList<Map<String,Double>>( clients.size() );
			
			
			for( Client c : clients ){
				Map<String,Double> stats = c.getAndClearStatistics();
				statistics.add( stats );
			}

			
			for( Map<String,Double> st : statistics )
				log.info( "{} : {}", date, st );
			
			try {Thread.sleep( 1000 ); } catch (Exception e) {}
		}
	}
	

    /**
     * @param args
     */
    public static void main(String[] args)
    throws Exception 
    {
        Properties p = new Properties();
        
        p.setProperty( "clients", "1,2" );
        
        
        p.setProperty( "client.1.url", "http://localhost/?ip=10.0.0.1" );
        p.setProperty( "client.1.rate", "220" );
        p.setProperty( "client.2.url", "http://localhost/does-not-exist?ip=172.16.0.1" );
        p.setProperty( "client.2.rate", "42" );
     
        
        
        if( p.get( "clients" ) == null ){
        	throw new Exception( "No clients defined!" );
        }
        
        
        List<Client> clients = new ArrayList<Client>();
        
        for( String id : p.getProperty( "clients" ).split( "," ) ){
        	Client c = new Client( new Integer(id.trim()), p.getProperty( "client." + id.trim() + ".url" ), new Double( p.getProperty( "client." + id.trim() + ".rate" ) ) );
        	clients.add( c );
        }
        
        PeriodicFetcher f = new PeriodicFetcher( clients );
        f.start();
        
        f.join();
    }
}