/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.tools;

import java.io.File;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;

public class EventCounter
implements CommandLineTool
{

    public String getName()
    {
        return "EventCounter";
    }


    public void run(String[] args) throws Exception
    {
        if( args.length != 1 ){
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println( "  org.jwall.tools.EventSender" );
            System.out.println( "  ---------------------------\n" );
            System.out.println( "  This command simply parses the given log-file and returns the number of events found." );
            System.out.println();
            System.out.println( "  Usage:" );
            System.out.println( "  \tjava -jar " + Tools.TOOL_JAR + "  count  AUDIT_LOG_FILE\n" );
            System.out.println();
            System.out.println( "  AUDIT_LOG_FILE simply is the name (and path to) of the file you want the count events in.");
            System.out.println();
            System.out.println();
            return;
        }
        
        File input = new File( args[0] );
        if( ! input.canRead() ){
            System.out.println( "Cannot open file \"" + input.getAbsolutePath() + "\" for reading!" );
            return;
        }

        AuditEventReader reader = AuditFormat.createReader( input.getAbsolutePath(), false );

        long start = System.currentTimeMillis();
        AuditEvent evt = reader.readNext();
        int cnt = 0;
        String lastId = evt.getEventId();

        while( evt != null ){
            cnt++;
            try {
                evt = reader.readNext();
                if( evt != null )
                    lastId = evt.getEventId();
            } catch (Exception e) {
                System.out.println( "Failed to parse event: " + e.getMessage() );
                System.out.println( "Last event successfully parsed was: " + lastId );
                break;
            }
        }
        long end = System.currentTimeMillis();

        System.out.println( "Counted " + cnt + " events in " + (end-start) + " ms." );        
    }


    /**
     * @param args
     */
    public static void main(String[] args)
    throws Exception
    {
        EventCounter counter = new EventCounter();
        counter.run( args );
    }

}
