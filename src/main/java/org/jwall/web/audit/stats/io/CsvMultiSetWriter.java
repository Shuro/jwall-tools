/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.web.audit.stats.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Iterator;

import org.jwall.util.MultiSet;

public class CsvMultiSetWriter implements MultiSetWriter {

	PrintStream p;

	public CsvMultiSetWriter( OutputStream out ){
		p = new PrintStream( out );
	}


	@Override
	public void write(MultiSet<String> row) throws IOException {
		if( row.getCount( "__TIME__" ) > 0.0d ){
			p.print( "@" + row.getCount( "__TIME__" ) );
			p.print( CsvMultiSetReader.SEPARATOR );
		}
		
		Iterator<String> it = row.getValues().iterator();
		while( it.hasNext() ){
			String k = it.next();
			if( !k.startsWith( "__" ) ){
				p.print( k );
				p.print( CsvMultiSetReader.SEPARATOR );
				p.print( row.getCount( k ) );
				if( it.hasNext() )
					p.print( CsvMultiSetReader.SEPARATOR );
				else
					p.println();
			}
		}
	}


	@Override
	public void close() throws IOException {
		flush();
		p.close();
	}


	@Override
	public void flush() {
		p.flush();
	}
}
