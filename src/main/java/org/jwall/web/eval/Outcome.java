/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.web.eval;


/**
 * <p>
 * An outcome instance describes the result of a test-case that has been injected into a server
 * and the observed values of the response. In the &quot;good&quot; case, the received value for
 * a specific response aspect will be contained and compared to the expected value.
 * </p>
 * <p>
 * Each instance of this class also has a specific ID associated, which is the ID of the test-case
 * for which it has been recorded.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class Outcome
    extends Expectation
{
    String id = "";
    
    String received;

    public Outcome( String id, String var, String exp, String recv ){
        super( var, exp );
        
        this.id = id;
        received = recv;
    }
    
    
    public Outcome( String id, String msg ){
        super( "GENERIC", "ERROR" );
        this.id = id;
        received = msg;
    }
    

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return the received
     */
    public String getReceived()
    {
        return received;
    }

    /**
     * @param received the received to set
     */
    public void setReceived(String received)
    {
        this.received = received;
    }
    
    public boolean success(){
        return this.getValue().equals( this.getReceived() );
    }
}
