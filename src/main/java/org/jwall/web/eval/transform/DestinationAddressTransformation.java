/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/*
 *  Copyright (C) 2007 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the TestClient application.
 *
 *  TestClient is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  TestClient is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.jwall.web.eval.transform;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventType;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.ModSecurityAuditEvent;

/**
 * 
 * This changes the destination address and optinally the destination port of
 * the AuditEvent.
 * 
 * @author chris@jwall.org
 * 
 */
public class DestinationAddressTransformation implements Transformation {
	String newAddress = "";
	Integer newPort = null;

	/**
	 * This creates a transformation that ensures the destination address of all
	 * processed auditevents to be <code>toAddress</code>. If
	 * <code>toAddress</code> contains a colon, everything following that colon
	 * will be used as the new port of the audit-events.
	 * 
	 * @param to
	 */
	public DestinationAddressTransformation(String toAddress) {
		this(toAddress, null);
	}

	public DestinationAddressTransformation(String toAddr, Integer port) {
		newAddress = toAddr;
		newPort = port;
	}

	public AuditEvent transform(AuditEvent evt) throws TransformationException {
		try {

			if (newAddress != null)
				evt.set(ModSecurity.SERVER_ADDR, newAddress);

			if (newPort != null)
				evt.set(ModSecurity.SERVER_PORT, newPort.toString());

			if (newAddress != null || newPort != null)
				return evt;

			String[] data = evt.getRawData();

			//
			// TODO: do the actual transformation on the section-data
			// 0 1 2 3 4 5 6
			// s = [... ..] id src sport dst dport

			String[] s = data[ModSecurity.SECTION_AUDIT_LOG_HEADER].split(" ");
			int idx = newAddress.indexOf(":");
			if (idx > 0) {
				s[5] = newAddress.substring(0, idx);
				s[6] = newAddress.substring(idx + 1);
			} else
				s[5] = newAddress;

			if (this.newPort != null)
				s[6] = newPort.toString();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < s.length; i++) {
				if (i > 0)
					sb.append(" ");
				sb.append(s[i]);
			}
			data[ModSecurity.SECTION_AUDIT_LOG_HEADER] = sb.toString() + "\n";

			return new ModSecurityAuditEvent(data, AuditEventType.ModSecurity2);
		} catch (Exception e) {
			throw new TransformationException(e.getMessage());
		}
	}

}
