/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.web.eval;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.http.HttpResponse;


/**
 *
 * This interface defines a simple logger, which is used to keep track of the
 * results of an injected event. The implementing classes may write these results
 * to XML or SQL databases, which can later be used to more precisely investigate
 * and compare the differences of the evaluated rulesets.
 * 
 * 
 * @author chris@jwall.org
 *
 */
public interface ResultLogger
{
    
    public String getEvaluationID();

    /**
     * This method is used to log the results of the injection of a specific audit-event.
     * The implementing class is responsible to extract the required information of the
     * injection from the response object.
     * 
     * The <code>run</code> string is used to identify this evaluation-run.
     * 
     * @param run A unique identifier to distinguish several evaluations.
     * @param evt The event that is injected.
     * @param response The result of the injection.
     */
    public void log( AuditEvent evt, HttpResponse response );
}
