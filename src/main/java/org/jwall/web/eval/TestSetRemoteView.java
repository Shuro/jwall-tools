/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.web.eval;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;

public class TestSetRemoteView
    implements TestSetView, TestSetDataView
{
    static Logger log = Logger.getLogger( TestSetRemoteView.class.getName() );
    
    URL url;
    
    String userName;
    
    String password;

    Map<Integer,String> rowToId = new HashMap<Integer,String>();
    
    Collection<Map<String,String>> cache = null;
    
    Map<String,HttpTestCase> dataCache = new HashMap<String,HttpTestCase>();
    
    public TestSetRemoteView( URL url, String user, String pass ){
        this.url = url;
        this.userName = user;
        this.password = pass;
    }
    
    
    public HttpTestCase getById( String id ){
        
        try {
            
            if( dataCache.containsKey( id ) )
                return dataCache.get( id );
            
            URL tcURL = new URL( url.getProtocol() + "://" + url.getHost() + ":" + url.getPort() + "" + url.getPath() + "/" + id );
            log.info( "Fetching test-case from URL "+ tcURL );
            
            
            HttpTestCase tc = (HttpTestCase) TestSet.getXStream().fromXML( tcURL.openStream() );
            dataCache.put( id, tc );
            return tc;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return null;
    }


    @SuppressWarnings("unchecked")
    public Collection<Map<String, String>> list()
    {
        Collection<Map<String,String>> list = new Vector<Map<String,String>>();
        
        
        if( cache != null )
            return cache;
        
        try {
            
            URLConnection con = url.openConnection();
            
            con.setDoOutput( true );
            con.setDoInput( true );
            con.setUseCaches( false );

            con.connect();
            
            
            PrintStream out = new PrintStream( con.getOutputStream() );
            out.println( "format=obj" );
            out.close();
            
            InputStream in = con.getInputStream();
            ObjectInputStream ois = new ObjectInputStream( in );
            list = (Collection<Map<String,String>>) ois.readObject();
            
            int i = 0;
            
            for( Map<String,String> entry : list ){
                
                if( entry.get( "ID") != null )
                    this.rowToId.put( i, entry.get( "ID" ) );
                
                i++;
            }
            
            cache = list;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return list;
    }
    
    
    public static void main( String args[] ) throws Exception {
        
        TestSetRemoteView view = new TestSetRemoteView( new URL("http://localhost:8080/WebAuditStorage/tc" ), null, null );
        HttpTestCase tc = view.getById( "1" );
        
        if( tc != null ){
            System.out.println( "Retrieved TC-1: " + tc ); 
        }
        
        
        Collection<Map<String,String>> list = view.list();
        System.out.println( "listing: " + list );
    }


    public HttpTestCase get( Integer i )
    {
        if( this.rowToId.get( i ) != null )
            return getById( rowToId.get( i ) );
        
        return null;
    }


    public int size()
    {
        Collection<Map<String,String>> list = list();
        if( list == null )
            return 0;
        
        return list.size();
    }
}
