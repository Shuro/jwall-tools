package org.jwall.web.eval;

import java.net.URL;
import java.util.Random;

import stream.util.URLUtilities;

public class RandomRequests {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		Random rnd = new Random( 1000L );

		for( int i = 0; i < 1; i++ ){
			String location = "http://modsecurity.fritz.box/";

			int len = rnd.nextInt( 1024 );
			if( len > 0 ){

				StringBuffer s = new StringBuffer();
				while( s.length() < len ){
					s.append( rnd.nextInt( 10 ) );
				}

				location += "?variable=" + s.toString();
			}

			try {
				System.out.println( "Sending request " + i + "  to " + location );
				URL url = new URL( location );
				URLUtilities.readContent(url);
			} catch (Exception e) {
				System.err.println( "Error: " + e.getMessage() );
				//e.printStackTrace();
			}
		}
	}
}
