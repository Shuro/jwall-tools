/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.web.eval;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;


/**
 * This abstract class defines all common methods for the test cases, such as the tag support, getters and setters
 * for name and comment.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public abstract class AbstractTestCase
    implements TestCase, TagSupport
{
    String comment;
    
    @XStreamAsAttribute
    String name;
    
    HashSet<String> tags = null;

    @XStreamImplicit
    LinkedList<Expectation> expectations = new LinkedList<Expectation>();

    @XStreamAlias("TestResult")
    TestResult result = null;

    public String getComment()
    {
        return comment;
    }

    public String getName()
    {
        return name;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setName(String title)
    {
        this.name = title;
    }

    public Set<String> getTags()
    {
        if( tags == null )
            return new HashSet<String>();
        
        return tags;
    }
    
    public void setTags( Set<String> newTags ){
        
        if( tags == null )
            tags = new HashSet<String>();
        else
            tags.clear();
        
        tags.addAll( newTags );
    }
    

    public void tag(String tag)
    {
        if( tags == null )
            tags = new HashSet<String>();
        
        tags.add( tag );
    }

    public void untag(String tag)
    {
        if( tags == null )
            return;
        
        tags.remove( tag );
    }
    
    public List<Expectation> getExpectations(){
        return this.expectations;
    }
    
    public void setResult( TestResult result ){
        this.result = result;
    }
}
