/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.web.eval;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;


/**
 * This class represents a set of test events. Each event is simply an audit-event and contains a HTTP
 * request, along with some user-defined expectations.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias( "TestSet" )
public class TestSet
    implements Collection<HttpTestCase>
{
    @XStreamImplicit
    LinkedList<HttpTestCase> events = new LinkedList<HttpTestCase>();

    
    public HttpTestCase get( int i ){
        return events.get( i );
    }
    
    
    /**
     * @see java.util.Collection#add(java.lang.Object)
     */
    public boolean add(HttpTestCase o)
    {
        return events.add( o );
    }

    /**
     * @see java.util.Collection#addAll(java.util.Collection)
     */
    public boolean addAll(Collection<? extends HttpTestCase> c)
    {
        return events.addAll( c );
    }

    /**
     * @see java.util.Collection#clear()
     */
    public void clear()
    {
        events.clear();
    }

    /**
     * @see java.util.Collection#contains(java.lang.Object)
     */
    public boolean contains(Object o)
    {
        return events.contains( o );
    }

    /**
     * @see java.util.Collection#containsAll(java.util.Collection)
     */
    public boolean containsAll(Collection<?> c)
    {
        return events.containsAll( c );
    }

    /**
     * @see java.util.Collection#isEmpty()
     */
    public boolean isEmpty()
    {
        return events.isEmpty();
    }

    /**
     * @see java.util.Collection#iterator()
     */
    public Iterator<HttpTestCase> iterator()
    {
        return events.iterator();
    }

    /**
     * @see java.util.Collection#remove(java.lang.Object)
     */
    public boolean remove(Object o)
    {
        return events.remove( o );
    }

    /**
     * @see java.util.Collection#removeAll(java.util.Collection)
     */
    public boolean removeAll(Collection<?> c)
    {
        return events.removeAll( c );
    }

    /**
     * @see java.util.Collection#retainAll(java.util.Collection)
     */
    public boolean retainAll(Collection<?> c)
    {
        return events.retainAll( c );
    }

    /**
     * @see java.util.Collection#size()
     */
    public int size()
    {
        return events.size();
    }

    /**
     * @see java.util.Collection#toArray()
     */
    public Object[] toArray()
    {
        return events.toArray();
    }
    
    /**
     * @see java.util.Collection#toArray(T[])
     */
    public <T> T[] toArray(T[] a)
    {
        return events.toArray( a );
    }
    
    
    
    public static XStream getXStream(){
        
        XStream xs = new XStream();
        xs.processAnnotations( TestSet.class );
        xs.processAnnotations( TestCase.class );
        xs.processAnnotations( HttpTestCase.class );
        xs.processAnnotations( Expectation.class );
        xs.processAnnotations( Message.class );
        xs.processAnnotations( TestResult.class );
        
        return xs;
    }
    
    public static TestSet read( InputStream in ) throws IOException {
        return (TestSet) getXStream().fromXML( in );
    }
    
    public static void write( TestSet tests, OutputStream out ) throws IOException {
        getXStream().toXML( tests, out );
    }
}
