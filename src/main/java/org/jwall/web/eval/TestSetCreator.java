/*******************************************************************************
 * Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
 *    
 *   This file is part of the jwall-tools. The jwall-tools is a set of Java
 *   based commands for managing ModSecurity related task such as counting
 *   events in audit-log files, generating HTML file from Apache configurations
 *   and other.
 *   More information and documentation for the jwall-tools can be found at
 *   
 *                      http://www.jwall.org/jwall-tools
 *   
 *   This program is free software; you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation; either version 3 of the License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License along with this 
 *   program; if not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jwall.web.eval;

import java.io.File;
import java.io.FileWriter;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;

import com.thoughtworks.xstream.XStream;


/**
 * 
 * This tool will read an audit-log file and create a new test-set based on the
 * event's server response. 
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class TestSetCreator
{
    static String[] EXPECT_VARS = {
      ModSecurity.RESPONSE_STATUS  
    };

    public static HttpTestCase create( AuditEvent evt ) throws Exception {
        
        String req = evt.get( ModSecurity.REQUEST_HEADER );
        String body = null;
        if( evt.isSet( ModSecurity.REQUEST_BODY ) )
            body = evt.get( ModSecurity.REQUEST_BODY );

        String addr = evt.get( ModSecurity.SERVER_ADDR );
        
        HttpTestCase tc = new HttpTestCase( addr, req, body );
        
        for( String var : EXPECT_VARS ){
            if( evt.isSet( var ) ){
                Expectation exp = new Expectation( var, evt.get( var ) );
                tc.expectations.add( exp );
            }
        }
        
        return tc;
    }
    
    
    /**
     * @param args
     */
    public static void main(String[] args)
        throws Exception
    {
        
        if( args.length < 1 ){
            
            System.out.println( "Usage:\n" );
            System.out.println( "\tjava org.jwall.web.eval.TestSetCreator  /path/to/audit-log  output-file" );
            System.out.println();
            
            return;
        }

        
        TestSet tests = new TestSet();
        
        File auditlog = new File( args[0] );
        
        AuditEventReader reader = AuditFormat.createReader( auditlog.getAbsolutePath(), false );
        
        int limit = 2000;
        AuditEvent evt = reader.readNext();
        
        while( evt != null && limit > 0 ){
            
            tests.add( TestSetCreator.create( evt ) );
            
            limit--;
            evt = reader.readNext();
        }
        
        XStream xs = new XStream();
        xs.processAnnotations( TestSet.class );
        xs.processAnnotations( TestCase.class );
        xs.processAnnotations( HttpTestCase.class );
        xs.processAnnotations( Expectation.class );
        
        xs.toXML( tests, new FileWriter( new File( auditlog.getName() + "-test.xml" ) ) );
    }
}
