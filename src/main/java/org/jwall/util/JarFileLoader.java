package org.jwall.util;

import java.io.File;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class JarFileLoader 
extends URLClassLoader 
{
	
	public JarFileLoader(){
		super( new URL[0] );
	}

	public JarFileLoader (URL[] urls)
	{
		super (urls);
	}

	public void addFile (String path) throws MalformedURLException
	{
		String urlPath = "jar:file://" + path + "!/";
		addURL (new URL (urlPath));
	}


	public static void addPath(String s) throws Exception {
		File f = new File(s);
		URL u = f.toURI().toURL();
		URLClassLoader urlClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
		Class<?> urlClass = URLClassLoader.class;
		Method method = urlClass.getDeclaredMethod("addURL", new Class[]{URL.class});
		method.setAccessible(true);
		method.invoke(urlClassLoader, new Object[]{u});
	}
	
	
	
	public static void main (String args [])
	{
		try
		{
			System.out.println ("First attempt...");
			Class.forName ("org.gjt.mm.mysql.Driver");
		}
		catch (Exception ex)
		{
			System.out.println ("Failed.");
		}

		try
		{
			URL urls [] = {};

			JarFileLoader cl = new JarFileLoader (urls);
			cl.addFile ("/opt/mysql-connector-java-5.0.4/mysql-connector-java-5.0.4-bin.jar");
			System.out.println ("Second attempt...");
			cl.loadClass ("org.gjt.mm.mysql.Driver");
			System.out.println ("Success!");
		}
		catch (Exception ex)
		{
			System.out.println ("Failed.");
			ex.printStackTrace ();
		}
	}
}
