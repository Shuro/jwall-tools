package org.jwall.util;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.Socket;
import java.net.URL;
import java.nio.channels.Channels;
import java.util.LinkedHashMap;
import java.util.Map;

import org.jwall.web.http.HttpResponse;
import org.jwall.web.http.nio.HttpResponseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * This client implements a simple thread which periodically calls a
 * URL at a given rate (requests per second). It also tracks the responses
 * for the calls.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class Client implements Runnable {
	static Logger log = LoggerFactory.getLogger( Client.class );
	Integer id;
	URL url;
	Double rate;
	Double delay;
	Integer error = 0;
	Integer success = 0;
	boolean running = false;

	public Client( Integer id, String urlString, Double rate ) throws Exception {
		this.id = id;
		this.url = new URL( urlString );
		this.rate = rate;
		this.delay = 1000.0d / rate;
		log.info( "Rate is {} requests per second => {} ms delay between requests", rate, delay );
	}


	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		log.info( "Starting client '{}'", id );
		running = true;
		int wait = delay.intValue();

		Socket socket = null;
		PrintStream out = null;
		HttpResponseChannel res = null;
		StringBuffer s = new StringBuffer();
		s.append( "GET " );
		s.append( url.getPath() );
		if( url.getQuery() != null ){
			s.append( "?" );
			s.append( url.getQuery() );
		}
		s.append( " HTTP/1.1\r\n" );

		s.append( "Host: " + url.getHost() + "\r\n");
		s.append( "Connection: keep-alive\r\n" );
		s.append( "\r\n" );
		s.append( "\r\n" );
		String request = s.toString();

		while( running ){

			try {


				if( socket == null || socket.isClosed() ){
					int port = url.getPort();
					if( port < 0 )
						port = 80;
					socket = new Socket( url.getHost(), port );

					out = new PrintStream( socket.getOutputStream() );
					res = new HttpResponseChannel( Channels.newChannel( socket.getInputStream() ) );
				}

				out.println( request );
				out.flush();
				HttpResponse response = res.readMessage();
				//log.info( "Connection: " + response.getHeader( "Connection" ) );
				//log.info( response.getBodyAsString() );
		        BufferedReader rr = new BufferedReader(new StringReader( response.getHeader() ) );
		        String rl = "";
		        do {
		            rl = rr.readLine();
		            if( rl != null && rl.matches("^HTTP/1\\.\\d.*") ){
		                String[] tl = rl.split(" ");
		                if( tl.length > 1 ){
		                }
		            }

		            /*
		            if( rl != null && rl.startsWith( "Location" ) ){
		            	String redir = rl.replaceFirst(".*: ", "");
		            }
		             */
		        } while ( rl != null );
				
				
				synchronized( success ){
					success++;
				}


				if( response.getHeader( "Connection" ).equalsIgnoreCase( "close" ) ){
					out.close();
					res.close();
					//socket.shutdownInput();
					//socket.shutdownOutput();
					socket = null;
				}

			} catch (Exception e) {
				e.printStackTrace();
				synchronized( error ){
					error++;
				}

				if( socket.isClosed() ){
					log.info( "Socket is closed!!! " );
					socket = null;
				}
			}

			try {
				//log.info( "Waiting {} ms before next request", wait );
				Thread.sleep( wait );
			} catch (Exception e) {
				e.printStackTrace();
			}

			if( success + error > 500 )
				running = false;
		}
	}


	public Map<String,Double> getAndClearStatistics(){
		Map<String,Double> stats = new LinkedHashMap<String,Double>();
		Long time = System.currentTimeMillis();
		stats.put( "time", new Double( time.doubleValue() ) );
		stats.put( "client.id", new Double(id.doubleValue()) );
		stats.put( "errors", new Double( error.intValue() ) );
		stats.put( "success", new Double( success.doubleValue() ) );
		error = 0;
		success = 0;
		return stats;
	}


	public void stop(){
		running = false;
	}


	public static void main( String[] args ) throws Exception {

		Client client = new Client( 1, "http://localhost/", 4.0d );
		Thread t = new Thread( client );
		t.start();

		log.info( "Waiting for client to finish..." );
		t.join();
	}
}