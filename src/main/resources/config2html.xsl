<?xml version="1.0" ?> 
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" omit-xml-declaration="yes"/>
<xsl:template match="/">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
   <title>..:: jwall.org :: ApacheConfig2Html ::..</title>
<style type="text/css">
body {
   font-family: Verdana;
   font-size: 10pt;
   padding-top: 10px;
}

a {
	text-decoration: none;
	color: #003366;
	}


div.header {
  background-image: url('http://www.jwall.org/images/jwall_background.png');
  background-repeat: repeat-x;
  background-position: center center;
  height: 97px;
  padding: 0px;
  margin: 0px;
  width:1000px;
//  border: solid blue 1px;
}


div.content {
	background-color: #ffffff; 
	padding:0px; 
	font-family: Verdana; 
	width:998px;	
	margin-top: 0px;
	margin-bottom: 0px;
	vertical-align: top;
}

div.wrapper {
  border-right:solid gray 1px; 
  border-left:solid gray 1px; 
  background-color: #ffffff; 
  text-align: middle;
  padding:0px; 
  font-family: Verdana; 
  width:998px;
}

div.toolbar {
  text-align: left;
  position: relative;
  width:993px;
  height: 22px;
  background-color: #ffffff;
  padding:0px;
  padding-left: 4px;
  margin: 0px;
}

div.footer {
  border: none;
  border-bottom:solid gray 1px; 
  margin-top: 20px;
  padding: 4px;
  border-top: solid #e0e0e0 1px;
  text-align: right;
  font-size: 8pt;
}

div.abstractTreeNode {
  margin-top: 4px;
  margin-left: 15px;
  vertical-align: middle;
}
</style>
<script language="javascript" type="text/javascript">
  function toggleVisibility ( id ) {
     var elem = document.getElementById( id );
     if( elem ){
       if( elem.style.display == 'none' )
          elem.style.display = 'block';
       else
          elem.style.display = 'none';
     }
  }
</script>
<style type="text/css">
  div.directive {
     padding-top: 5px;
     padding-bottom: 5px;
  }
  
  p.directive {
  	margin-left: 10px;
  }
  
  p.SecRule {
    
  }
  p.SecRule:hover {
     background: yellow;
  }

  div.ApacheConfig {
     border: solid 1px #ffffff;
  }
  div.ApacheConfig:hover {
     border: solid 1px #e0e0e0;
  }
</style>
</head>

<body style="background-color:#e0e0e0;" align="center">

<div align="center" style="background-color: #e0e0e0;">

  <div class="header">
  </div>

  <div align="center" class="wrapper">

  <div class="content">
	<xsl:apply-templates/>
  </div>
  
  <div class="footer">
    <a target="_blank" href="http://www.jwall.org/">jwall.org ToolBox</a>, Apache2Html 0.2
  </div>
</div>

</div>
  </body>
</html>

</xsl:template>

<xsl:template match="ApacheConfig">
<xsl:variable name="file"><xsl:value-of select="@file" /></xsl:variable>
<xsl:variable name="myId"><xsl:value-of select="generate-id(.)" /></xsl:variable>
<div align="left" valign="top" style="margin: 0px; margin-left: 4px; padding: 10px;" class="ApacheConfig">
  #file: <xsl:value-of select="@file" /> <xsl:text>     </xsl:text> <a href="javascript:toggleVisibility('{$myId}')">Toggle</a>
<div id="{$myId}" style="display:none; padding: 0px; margin: 0px;">
<code style="padding: 0px; margin: 0px; padding-left:20px; ">
 <xsl:apply-templates/>      
</code>
</div>
</div>
</xsl:template>

<xsl:template match="Comment">
<pre style="color: #b0b0b0;"><xsl:value-of select="text()"/></pre>
</xsl:template>

<xsl:template match="Files|Location|Directory">
<div class="directive">
&lt;<xsl:value-of select="name(.)"/> path=&quot;<xsl:value-of select="@path"/>&quot;&gt;
<div style="margin-left: 20px;">
    <xsl:apply-templates />
</div>
&lt;/<xsl:value-of select="name(.)"/>&gt;
</div>
</xsl:template>

<xsl:template match="IfModule">
<div class="directive">
&lt;IfModule name=&quot;<xsl:value-of select="@name"/>&quot;&gt; 
<div style="margin-left: 10px;">
  <xsl:apply-templates />
</div>
&lt;/IfModule&gt;
</div>
</xsl:template>


<xsl:template match="VirtualHost">
<div style="padding-top: 20px;">
  &lt;<xsl:value-of select="name(.)"/><xsl:text> </xsl:text><xsl:value-of select="@address"/>&gt;
  <div style="padding-left: 20px;">
  <xsl:apply-templates />
  </div>
  &lt;/<xsl:value-of select="name(.)"/>&gt;
</div>
</xsl:template>


<xsl:template match="FilesMatch|LocationMatch|DirectoryMatch">
<div>
&lt;<xsl:value-of select="name(.)"/> &quot;<xsl:value-of select="@pattern"/>&quot;&gt; 
 <div style="padding-left: 20px;">
    <xsl:apply-templates />
 </div>
&lt;<xsl:value-of select="name(.)"/>&gt;
</div>
</xsl:template>


<xsl:template 
match="DefaultType|AddDefaultCharset|AddHandler|AddEncoding|SSLRandomSeed|IndexOptions|Alias|AddIconByEncoding|AddIconByType|LoadModule|LoadFile|Listen|User|Group|ServerName|UseCanonicalNames|UserDir|ServerAlias|Options|AllowOverride|Order|Deny|Allow|ServerRoot|DocumentRoot|ServerAdmin|DirectoryIndex|ScriptAlias|ScriptAliasMatch|Scriptsock|AddType|TypesConfig|Satisfy|ErrorDocument|Include|AddIcon|DefaultIcon|LogFormat|ErrorLog|AccessLog|LogLevel|CustomLog|ReadmeName|HeaderName|AddDescription|IndexIgnore|AddLanguage|LanguagePriority|ForceLanguagePriority|AddCharset|AliasMatch|SetHandler|RedirectMatch|SetEnvIf|PidFile|ForceLanguagePriority|MinSpareThreads|MaxSpareThreads|StartServers|MaxClients|ThreadLimit|ThreadsPerChild|MaxRequestsPerChild|MinSpareServers|MaxSpareServers|ThreadStackSize|StartThreads|MaxRequestsPerThread|MaxThreads|MaxMemFree|KeepAlive|MaxKeepAliveRequests|KeepAliveTimeout|Timeout|LockFile|AccessFileName|HostnameLookups|ServerTokens|ServerSignature|ErrorDocument|MIMEMagicFile|AddOutputFilter|EnableMMAP|EnableSendFile|BrowserMatch|Redirect">
<p>
  <xsl:value-of select="name(.)"/><xsl:text> </xsl:text><xsl:value-of select="text()" />
  <xsl:for-each select="./attribute::name">
     <xsl:value-of select="." />
  </xsl:for-each>
</p>
</xsl:template>

<xsl:template match="Include">
<p style="padding-left: 0px; padding-top: 10px;">
   Include <xsl:value-of select="@pattern"/>  <br/>
   
<div valign="top" style="padding: 0px; margin: 0px; margin-left: 40px;">
  	<xsl:apply-templates />
</div>
</p>
</xsl:template>


<xsl:template match="ServerAlias">
<p>
  <xsl:value-of select="name(.)"/><xsl:text> </xsl:text><xsl:value-of select="text()" />
  <xsl:for-each select="./attribute::name">
     <xsl:value-of select="." />
  </xsl:for-each>
</p>
</xsl:template>


<xsl:template match="NameVirtualHost">
<p>
  <xsl:value-of select="name(.)"/><xsl:text> </xsl:text><xsl:value-of select="text()" />
  <xsl:for-each select="./attribute::name">
     <xsl:value-of select="." />
  </xsl:for-each>
</p>
</xsl:template>

<xsl:template match="Proxy">
<p>
  &lt;<xsl:value-of select="name(.)"/><xsl:text> </xsl:text><xsl:value-of select="@address"/>&gt;
  <div style="padding-left: 20px;">
    <xsl:apply-templates />
  </div>
  &lt;/<xsl:value-of select="name(.)"/>&gt;
</p>
</xsl:template>

<xsl:template match="ProxyPass|ProxyPassReverse|ProxyPreserveHost|ProxyRequests|ProxyVia">
<p>
  <xsl:value-of select="name(.)"/><xsl:text> </xsl:text><xsl:value-of select="text()" />
  <xsl:for-each select="./attribute::name">
     <xsl:value-of select="." />
  </xsl:for-each>
</p>
</xsl:template>


<xsl:template match="SecMarker|SecRuleEngine|SecRequestBodyAccess|SecRequestBodyLimit|SecResponseBodyAccess|SecResponseBodyLimit|SecArgumentSeparator|SecAuditLogParts|SecAuditEngine|SecAuditLog|SecAuditLog2|SecAuditLogStorageDir|SecAuditLogType|SecDebugLogLevel|SecDebugLog|SecDataDir|SecTmpDir|SecRelevantStatus|SecAuditEngine|SecServerSignature">
<p>
  <xsl:value-of select="name(.)"/><xsl:text> </xsl:text><xsl:value-of select="text()" />
  <xsl:for-each select="./attribute::name">
     <xsl:value-of select="." />
  </xsl:for-each>
</p>
</xsl:template>

<xsl:template match="SecAction">
<p>SecDefaultAction<xsl:text> </xsl:text> <nobr><xsl:value-of select="./Actions" /></nobr></p>
</xsl:template>

<xsl:template match="SecDefaultAction">
<p>SecDefaultAction<xsl:text> </xsl:text> <nobr><xsl:value-of select="./Actions" /></nobr></p>
</xsl:template>

<xsl:template match="SecRule">
<xsl:variable name="id"><xsl:value-of select="@id"/></xsl:variable>
<p class="SecRule"><xsl:if test="@id != ''"><a name="{$id}"/></xsl:if>SecRule<xsl:text> </xsl:text> <nobr><xsl:value-of select="./Selector"/></nobr><xsl:text> </xsl:text><nobr><xsl:value-of select="./Operator" /></nobr> 
<xsl:text> </xsl:text>
<xsl:for-each select="./Actions/string">
   <xsl:value-of select="text()"/><xsl:if test="position() != last()">,</xsl:if>
</xsl:for-each>
</p>
</xsl:template>


</xsl:stylesheet>
