package org.jwall.tools;

public class MlogcTest
{

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        String[] params = new String[]{
                "/opt/modsecurity/etc/mlogc.conf", "/opt/modsecurity/var/audit/index"
        };
        
        Mlogc mlogc = new Mlogc();
        mlogc.run( params );
    }
}
