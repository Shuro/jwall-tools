#
#  Copyright (C) 2010 Christian Bockermann <chris@jwall.org>
#   
#  This file is part of the jwall-tools. The jwall-tools is a set of Java
#  based commands for managing ModSecurity related task such as counting
#  events in audit-log files, generating HTML file from Apache configurations
#  and other.
#  More information and documentation for the jwall-tools can be found at
#  
#                     http://www.jwall.org/jwall-tools
#  
#  This program is free software; you can redistribute it and/or modify it under
#  the terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 3 of the License, or (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License along with this 
#  program; if not, see <http://www.gnu.org/licenses/>.
#
#
#  This Makefile contains all instructions to create RPM and DEB packages
#  for the jwall-tools.
#
#
VERSION=0.5.5
REVISION=0
NAME=jwall-tools
BUILD=build_tmp
DEB_FILE=${NAME}-${VERSION}-${REVISION}.deb
DIST=jwall
DEB_DIST=jwall
RPM_FILE=${NAME}-${VERSION}-${REVISION}.noarch.rpm
RPMBUILD=$(PWD)/.rpmbuild
ARCH=noarch
RELEASE_DIR=releases

all:  assembly rpm deb zip sign-releases


assembly:
	rm -rf ${RELEASE_DIR}
	mkdir -p ${RELEASE_DIR}
	mvn package
	cp target/jwall-tools-${VERSION}.jar ${RELEASE_DIR}/jwall-tools-${VERSION}-${REVISION}.jar


rpm:
	mkdir -p ${RELEASE_DIR}
	rm -rf ${RPMBUILD}
	mkdir -p ${RPMBUILD}
	mkdir -p ${RPMBUILD}/tmp
	mkdir -p ${RPMBUILD}/RPMS
	mkdir -p ${RPMBUILD}/RPMS/${ARCH}
	mkdir -p ${RPMBUILD}/BUILD
	mkdir -p ${RPMBUILD}/SRPMS
	rm -rf ${RPMBUILD}/BUILD
	mkdir -p ${RPMBUILD}/BUILD
	cp -a dist/opt ${RPMBUILD}/BUILD
	mkdir -p ${RPMBUILD}/SPECS
	mkdir -p ${RPMBUILD}/BUILD/opt/modsecurity/lib
	cp $(PWD)/target/jwall-tools-${VERSION}.jar ${RPMBUILD}/BUILD/opt/modsecurity/lib/jwall-tools.jar
	cp -a dist/jwall-tools.spec ${RPMBUILD}/SPECS
	find .rpmbuild/BUILD -type f | sed -e s/^\.rpmbuild\\/BUILD// | grep -v DEBIAN > ${RPMBUILD}/tmp/rpmfiles.list
	rpmbuild --target noarch --sign --define '_topdir ${RPMBUILD}' --define '_version ${VERSION}' --define '_revision ${REVISION}' -bb ${RPMBUILD}/SPECS/jwall-tools.spec --buildroot ${RPMBUILD}/BUILD
	cp ${RPMBUILD}/RPMS/${ARCH}/${RPM_FILE} ${RELEASE_DIR}

release-rpm:
	mkdir -p /var/www/download.jwall.org/htdocs/yum/${DIST}/noarch
	cp ${RELEASE_DIR}/${RPM_FILE} /var/www/download.jwall.org/htdocs/yum/${DIST}/noarch/
	createrepo /var/www/download.jwall.org/htdocs/yum/${DIST}/



deb:
	mkdir -p ${BUILD}/opt/modsecurity/bin
	mkdir -p ${BUILD}/opt/modsecurity/lib
	mkdir -p ${BUILD}/DEBIAN
	cp dist/DEBIAN/* ${BUILD}/DEBIAN/
	chmod 755 ${BUILD}/DEBIAN/p*
	cat dist/DEBIAN/control | sed -e 's/Version:.*/Version: ${VERSION}-${REVISION}/' > ${BUILD}/DEBIAN/control
	cp src/main/bin/* ${BUILD}/opt/modsecurity/bin
	cd ${BUILD} && find opt -type f -exec md5sum {} \; > DEBIAN/md5sums && cd ..
	cp target/jwall-tools-${VERSION}.jar ${BUILD}/opt/modsecurity/lib/jwall-tools.jar
	dpkg -b ${BUILD} ${RELEASE_DIR}/${DEB_FILE}
	#rm -rf ${BUILD}
	debsigs --sign=origin --default-key=C5C3953C ${RELEASE_DIR}/${DEB_FILE}

release-deb:
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian includedeb ${DEB_DIST} ${RELEASE_DIR}/${DEB_FILE}


unrelease-deb:
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian remove ${DEB_DIST} jwall-tools




zip:	assembly
	mkdir -p ${BUILD}/zip/opt/modsecurity/bin
	mkdir -p ${BUILD}/zip/opt/modsecurity/lib
	cp src/main/bin/* ${BUILD}/zip/opt/modsecurity/bin
	cp target/jwall-tools-${VERSION}.jar ${BUILD}/zip/opt/modsecurity/lib/jwall-tools.jar
	cd ${BUILD}/zip && zip -r ../../${RELEASE_DIR}/${NAME}-${VERSION}-${REVISION}.zip .
	rm -rf ${BUILD}/zip
	

clean:	clean-rpm clean-deb clean-zip
	mvn clean

clean-rpm:
	rm -f ${RPM_FILE}


clean-deb:
	rm -rf ${BUILD}
	rm -f ${DEB_FILE}

clean-zip:
	rm -rf ${BUILD}/zip
	rm -f ${NAME}-${VERSION}.zip

sign-releases:
	sh src/main/assembly/sign-releases.sh ./${RELEASE_DIR}

upload:
	cd ${RELEASE_DIR} && scp * chris@jwall.org:/var/www/download.jwall.org/htdocs/jwall-tools/${VERSION}/
	rm -f /var/www/download.jwall.org/htdocs/jwall-tools/latest-jwall-tools.jar
	ln -s /var/www/download.jwall.org/htdocs/jwall-tools/${VERSION}/jwall-tools-${VERSION}-${REVISION}.jar /var/www/download.jwall.org/htdocs/jwall-tools/latest-jwall-tools.jar


release: upload
